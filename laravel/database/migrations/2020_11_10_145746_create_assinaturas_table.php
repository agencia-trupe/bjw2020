<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAssinaturasTable extends Migration
{
    public function up()
    {
        Schema::create('assinaturas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->foreign('cadastro_id')->references('id')->on('cadastros')->onDelete('cascade');
            $table->text('mensagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('assinaturas');
    }
}
