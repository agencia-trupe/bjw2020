<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEventoTable extends Migration
{
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->string('realizacao');
            $table->string('site');
            $table->string('telefone');
            $table->text('texto_sobre');
            $table->text('texto_sobre_en');
            $table->text('frase_sobre');
            $table->text('frase_sobre_en');
            $table->string('evento_duracao');
            $table->string('evento_duracao_en');
            $table->text('frase_inicio');
            $table->text('frase_inicio_en');
            $table->string('frase_agenda');
            $table->string('frase_agenda_en');
            $table->string('frase_saudacao');
            $table->string('frase_saudacao_en');
            $table->string('evento_periodo');
            $table->string('evento_periodo_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('evento');
    }
}
