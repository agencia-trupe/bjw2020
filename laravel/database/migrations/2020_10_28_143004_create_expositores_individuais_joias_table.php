<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateExpositoresIndividuaisJoiasTable extends Migration
{
    public function up()
    {
        Schema::create('expositores_individuais_joias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('individual_id')->unsigned();
            $table->foreign('individual_id')->references('id')->on('expositores_individuais')->onDelete('cascade');
            $table->string('nome_joia');
            $table->string('nome_joia_en');
            $table->text('descricao');
            $table->text('descricao_en');
            $table->string('imagem1')->nullable();
            $table->string('imagem2')->nullable();
            $table->string('imagem3')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('expositores_individuais_joias');
    }
}
