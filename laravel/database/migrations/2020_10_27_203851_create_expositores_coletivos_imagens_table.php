<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateExpositoresColetivosImagensTable extends Migration
{
    public function up()
    {
        Schema::create('expositores_coletivos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('coletivo_id')->unsigned();
            $table->foreign('coletivo_id')->references('id')->on('expositores_coletivos')->onDelete('cascade');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('expositores_coletivos_imagens');
    }
}
