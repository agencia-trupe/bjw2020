<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateExpositoresIndividuaisProcessoTable extends Migration
{
    public function up()
    {
        Schema::create('expositores_individuais_processo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('individual_id')->unsigned();
            $table->foreign('individual_id')->references('id')->on('expositores_individuais')->onDelete('cascade');
            $table->string('video')->nullable();
            $table->string('imagem1')->nullable();
            $table->string('imagem2')->nullable();
            $table->string('imagem3')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('expositores_individuais_processo');
    }
}
