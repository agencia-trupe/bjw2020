<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateExpositoresIndividuaisTable extends Migration
{
    public function up()
    {
        Schema::create('expositores_individuais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('nome');
            $table->integer('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('whatsapp');
            $table->string('instagram');
            $table->string('email');
            $table->string('website')->nullable();
            $table->string('capa');
            $table->string('foto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('expositores_individuais');
    }
}
