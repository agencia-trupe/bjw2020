<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('evento')->insert([
            'nome'              => 'Brazil Jewelry Week',
            'data_inicio'       => Carbon::parse('2020-12-03 00:00')->format('Y-m-d h:m'),
            'data_fim'          => Carbon::parse('2020-12-22 23:59')->format('Y-m-d h:m'),
            'realizacao'        => '.',
            'site'              => 'www.braziljewelryweek.com.br',
            'telefone'          => '+55 11 98765 4321',
            'texto_sobre'       => 'Sobre o evento',
            'texto_sobre_en'    => 'Sobre o evento - ingles',
            'frase_sobre'       => '19 dias,  24 horas , 70 joalheiros e 20 palestrantes compõe a segunda edição do Brazil Jewelry Week.',
            'frase_sobre_en'    => '19 days, 24 hours, 70 jewelers and 20 speakers make up the second edition of Brazil Jewelry Week.',
            'evento_duracao'    => '20 DIAS • 24 HORAS',
            'evento_duracao_en' => '20 DAYS • 24 HOURS',
            'frase_inicio'      => 'Joalheiros Contemporâneos principalmente da América Latina apresentam seus olhares sobre a joia arte sobre o tema incorporar a obra.',
            'frase_inicio_en'   => 'Contemporary jewelers mainly from Latin America present their views on jewel art under the theme of incorporating the work.',
            'frase_agenda'      => 'Além de uma agenda on-line repleta de palestras.',
            'frase_agenda_en'   => 'In addition to an online schedule full of lectures.',
            'frase_saudacao'    => 'BOM EVENTO!',
            'frase_saudacao_en' => 'WE WISH YOU A GOOD EVENT!',
            'evento_periodo'    => '3 a 22 dezembro 2020',
            'evento_periodo_en' => 'From 3 to 22 december 2020',
        ]);
    }
}
