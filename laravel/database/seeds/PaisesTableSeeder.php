<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaisesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('paises')->insert([
            'id'      => 1,
            'nome' => 'Argentina',
            'nome_en' => 'Argentina',
        ]);

        DB::table('paises')->insert([
            'id'      => 2,
            'nome' => 'Brasil',
            'nome_en' => 'Brazil',
        ]);

        DB::table('paises')->insert([
            'id'      => 3,
            'nome' => 'Chile',
            'nome_en' => 'Chile',
        ]);

        DB::table('paises')->insert([
            'id'      => 4,
            'nome' => 'Colômbia',
            'nome_en' => 'Colombia',
        ]);

        DB::table('paises')->insert([
            'id'      => 5,
            'nome' => 'Espanha',
            'nome_en' => 'Spain',
        ]);

        // DB::table('paises')->insert([
        //     'id'      => 6,
        //     'nome' => 'Irã',
        //     'nome_en' => 'Will',
        // ]);

        DB::table('paises')->insert([
            'id'      => 7,
            'nome' => 'Itália ',
            'nome_en' => 'Italy',
        ]);

        DB::table('paises')->insert([
            'id'      => 8,
            'nome' => 'México',
            'nome_en' => 'Mexico',
        ]);

        DB::table('paises')->insert([
            'id'      => 9,
            'nome' => 'Portugal',
            'nome_en' => 'Portugal',
        ]);

        DB::table('paises')->insert([
            'id'      => 10,
            'nome' => 'Outros',
            'nome_en' => 'Others',
        ]);

    }
}
