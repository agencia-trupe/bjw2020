import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(window).on("load", function () {
  // OK - IMAGENS JOIAS - SLICK CAROUSEL
  $(".imgs-joias").slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    fade: true,
  });

  $(".loading").hide();
  $(".joias").css({ visibility: "visible", "max-height": "100%" });
});

$(document).ready(function () {
  // OK - CRONOMETRO PARA FIM DO EVENTO
  var dataFim = $(".data-fim").html();
  $(".cronometro-evento").countdown(dataFim, function (event) {
    $(".x-dias").html(event.strftime("%-D"));
    $(".x-horas").html(event.strftime("%-H"));
    $(".x-minutos").html(event.strftime("%-M"));
  });

  // OK - IDIOMAS
  $(".idiomas-titulo").click(function () {
    $(".idiomas-links").css("display", "block");
  });

  // OK - VARIAÇÕES IMGS CAMPANHA
  $(".campanha .variacao").click(function (e) {
    e.preventDefault();
    var linkAtivo = $("#variacaoMaior").attr("href");
    var src = $(this).attr("href");

    // removendo da variacao
    $(this).attr("href", linkAtivo);
    $(this).children().attr("src", linkAtivo);

    // adicionando a variacao-ativa
    $("#variacaoMaior").attr("href", src);
    $("#variacaoMaior img").attr("src", src);
  });

  // OK - BTN VER MAIS - AGENDA COMPLETA
  var itensAgendaCompleta = $(".agenda-geral");
  var spliceItensAgenda = 2;
  if (itensAgendaCompleta.length <= spliceItensAgenda) {
    $(".btn-agenda-mais").hide();
  }
  var setDivAgenda = function () {
    var spliceItens = itensAgendaCompleta.splice(0, spliceItensAgenda);
    $(".agenda").append(spliceItens);
    $(spliceItens).show();
    if ($(spliceItens[0]).length > 0) {
      $("html,body .agenda").animate({ scrollTop: $(spliceItens[0]).offset().top }, "slow");
    }
    if (itensAgendaCompleta.length <= 0) {
      $(".btn-agenda-mais").hide();
    }
  };
  $(".btn-agenda-mais").click(function () {
    setDivAgenda();
  });
  $(".agenda-geral").hide();
  setDivAgenda();

  // OK - MODAL LOGIN / ESQUECI MINHA SENHA / CADASTRO
  $("#btnModalLogin").click(function () {
    $("#modalLogin").css("display", "flex");
    $("#modalEsqueciMinhaSenha").css("display", "none");
    $("#modalCadastro").css("display", "none");
  });

  $("#btnEsqueciMinhaSenha").click(function () {
    $("#modalLogin").css("display", "none");
    $("#modalEsqueciMinhaSenha").css("display", "flex");
  });

  $("#btnCadastrar").click(function () {
    $("#modalLogin").css("display", "none");
    $("#modalEsqueciMinhaSenha").css("display", "none");
    $("#modalCadastro").css("display", "flex");
  });

  $(".btn-close").click(function () {
    $("#modalLogin").css("display", "none");
    $("#modalEsqueciMinhaSenha").css("display", "none");
    $("#modalCadastro").css("display", "none");
    $("#modalCompartilhar").css("display", "none");
  });

  // OK - COMPARTILHAR
  $(".compartilhar-joia").click(function () {
    $("#modalCompartilhar").css("display", "flex");
    $("#modalCompartilhar").css("position", "fixed");
  });

  // OK - USUARIO EVENTO DESLOGADO
  $(".link-deslogado").click(function (e, auth) {
    e.preventDefault();
    var auth = $(".itens-menu .logado-nome").html();
    if (auth == "" || auth == undefined) {
      $("#modalLogin").css("display", "flex");
      $("#modalLogin").css("position", "fixed");
    }
  });

  $("#btnAssinarLivro").click(function () {
    $(".pag-assine-livro > form > textarea").css("display", "none");
    $(".assinatura-enviada").css("display", "flex");
  });

  // OK - GRID ASSINATURAS
  var $grid = $(".masonry-grid")
    .masonry({
      itemSelector: ".grid-item",
      columnWidth: ".grid-item",
      percentPosition: true,
    })
    .on("layoutComplete", function (event, laidOutItems) {
      $(".masonry-grid").masonry("layout");
    });

  // OK - BTN VER MAIS ASSINATURAS
  var itensAssinaturas = $(".grid-item");
  var spliceItensAssinaturas = 10;

  if (itensAssinaturas.length <= spliceItensAssinaturas) {
    $(".btn-assinaturas-mais").hide();
  }

  var setDivAssinatura = function () {
    var spliceItens = itensAssinaturas.splice(0, spliceItensAssinaturas);
    $(".masonry-grid").append(spliceItens);
    $(spliceItens).show();
    $(".masonry-grid").masonry("layout");
    if (itensAssinaturas.length <= 0) {
      $(".btn-assinaturas-mais").hide();
    }
  };

  $(".btn-assinaturas-mais").click(function () {
    setDivAssinatura();
  });

  $(".grid-item").hide();
  setDivAssinatura();

  // var virtualPath = "";
  var virtualPath = "/2020_acesso_BJW";
  // var virtualPath = "/previa-braziljewelryweek";

  // AJAX para buscar os expositores na Controller (passando ID do país)
  var getExpositores = function (pais, show) {
    var url = virtualPath + "/expositores/" + pais;
    console.log(url);
    $.ajax({
      type: "GET",
      url: url,
      beforeSend: function () {
        $("#IndividualPais").html("");
        $("#ColetivoPais").html("");
      },
      success: function (data, textStatus, jqXHR) {
        var option = "<option value='' selected>" + translations.filtroIndividuais + "</option>";
        $("#IndividualPais").append(option);

        data.individual.forEach((element) => {
          option = '<option value="' + element.slug + '">' + element.nome + "</option>";
          $("#IndividualPais").append(option);
        });

        option = "<option value='' selected>" + translations.filtroColetivos + "</option>";
        $("#ColetivoPais").append(option);

        data.coletivo.forEach((element) => {
          option = '<option value="' + element.slug + '">' + element.nome + "</option>";
          $("#ColetivoPais").append(option);
        });
        
        if (show) {
          showExpositores(data.expositores, data.paises);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  };

  // Todos os expositores (sem escolher o país, $pais_id == 0)
  if (window.location.pathname.indexOf("filtro") < 0) {
    getExpositores(0, true);
  } else {
    var pais = window.location.pathname.replace(virtualPath + "/filtro/", "");
    if (pais != "0") {
      $("select#paises").val(pais);
    }
    getExpositores(pais, true);
  }

  // Todos os expositores (passando $pais_id)
  $("select#paises").on("change", function () {
    var pais = this.value;
    getExpositores(pais, false);
  });

  $("select#IndividualPais, select#ColetivoPais").on("change", function () {
    window.location.href = virtualPath + "/expositor/" + this.value;
  });

  //
  $("#lupaFiltro").on("click", function (e) {
    e.preventDefault();
    var pais = $("select#paises option:selected").val();
    getExpositores(pais, true);
  });

  $("#lupaHome, #lupaGeral").on("click", function (e) {
    e.preventDefault();
    var pais = $("select#paises option:selected").val();
    window.location.href = virtualPath + "/filtro/" + (pais == "" ? "0" : pais);
  });

  // Chama método que preeche o html com os expositores e seta intervalo para randomizar
  var showExpositores = function (expositores, paises) {
    // Preenche expositores a primeira vez, com 8 resultados, sem escolher o país
    if (window.location.pathname.indexOf("filtro") < 0) {
      fillExpositores(expositores.slice(0, 8), paises);
    } else {
      fillExpositores(expositores, paises);
    }

    // Seta intervalo para troca dos resultados exibidos, com método pra randomizar
    var intervalId = setInterval(function () {
      if (window.location.pathname.indexOf("filtro") < 0) {
        $("#todosExpositores").fadeOut(1000, "swing", function () {
          randomExpositores(expositores, 8, paises);
        });
      }
    }, 10000);
  };

  // Preenche o html dos expositores (com ou sem parâmetro)
  var fillExpositores = function (expositores, paises) {
    $("#todosExpositores").html("").css({ display: "none", opacity: 0 });

    expositores.forEach((element) => {
      var paisNome;

      paises.forEach((pais) => {
        if (element.pais_id == pais.id) {
          if (translations.linguagem == "pt") {
            paisNome = pais.nome;
          }
          if (translations.linguagem == "en") {
            paisNome = pais.nome_en;
          }
        }
      });
      
      var html = "<a href='" + window.location.origin + virtualPath + "/expositor/" + element.slug + "' class='expositor' title='" + element.nome + "'><img src='" + window.location.origin + "/assets/img/expositores/" + element.capa + "' alt='" + element.nome + "'><p class='expositor-nome'>" +
        element.nome + "</p><p class='expositor-pais'>" + paisNome + "</p></a>";
      $("#todosExpositores").append(html);
    });
    $("#todosExpositores").show().animate({ opacity: 1 }, 1600, "swing");
  };

  // Randomiza os expositores e chama o método que preenche o html dos expositores
  var randomExpositores = function (arr, n, paises) {
    var result = new Array(n);
    var len = arr.length;
    var taken = new Array(len);

    if (n > len)
      throw new RangeError("getRandom: more elements taken than available");

    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }

    fillExpositores(result, paises);
  };
});
