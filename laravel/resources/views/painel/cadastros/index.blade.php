@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Cadastros do Evento
    </h2>
</legend>


@if(!count($cadastros))
<div class="alert alert-warning" role="alert">Nenhum cadastro encontrada.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="cadastros">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>País</th>
            <th>E-mail</th>
            <th>Telefone</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($cadastros as $cadastro)
        <tr class="tr-row" id="{{ $cadastro->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($cadastro->created_at)) }}</td>
            <td>{{ $cadastro->nome }}</td>
            <td>{{ $cadastro->pais }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $cadastro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $cadastro->email }}
            </td>
            <td>{{ $cadastro->telefone }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection