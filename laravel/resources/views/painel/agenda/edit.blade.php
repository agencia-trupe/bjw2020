@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Agenda /</small> Editar Data</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.agenda.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.agenda.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
