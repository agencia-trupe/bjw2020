@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('horario', 'Horario (Exemplo: 9h)') !!}
    {!! Form::text('horario', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo', 'Tipo da Palestra (Exemplo: Visita guiada pela exposição)') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo_en', 'Tipo da Palestra (Exemplo: Visita guiada pela exposição) - (INGLÊS)') !!}
    {!! Form::text('tipo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título da Palestra') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título da Palestra (INGLÊS)') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome do Apresentador') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição da Palestra') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_en', 'Descrição da Palestra (INGLÊS)') !!}
    {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (Transmissão Zoom)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('id_reuniao', 'Id da Reunião') !!}
    {!! Form::text('id_reuniao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('senha_acesso', 'Senha de Acesso') !!}
    {!! Form::text('senha_acesso', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/agenda/'.$palestra->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.agenda.palestras.index', $registro->id) }}" class="btn btn-default btn-voltar">Voltar</a>
