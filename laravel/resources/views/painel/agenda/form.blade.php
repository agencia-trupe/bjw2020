@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dia_evento', 'Dia do Evento (Exemplo: Dia 1 • Quinta-feira)') !!}
    {!! Form::text('dia_evento', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dia_evento_en', 'Dia do Evento (Exemplo: Dia 1 • Quinta-feira) - (INGLÊS)') !!}
    {!! Form::text('dia_evento_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.agenda.index') }}" class="btn btn-default btn-voltar">Voltar</a>
