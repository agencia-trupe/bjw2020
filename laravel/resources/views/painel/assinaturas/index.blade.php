@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Assinaturas no Livro do Evento
    </h2>
</legend>


@if(!count($assinaturas))
<div class="alert alert-warning" role="alert">Nenhuma assinatura encontrada.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="assinaturas">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>País</th>
            <th>Mensagem</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($assinaturas as $assinatura)
        <tr class="tr-row" id="{{ $assinatura->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($assinatura->created_at)) }}</td>
            <td>{{ $assinatura->nome }}</td>
            <td>{{ $assinatura->pais }}</td>
            <td>{{ $assinatura->mensagem }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.assinaturas-livro.destroy', $assinatura->id],
                'method' => 'delete'
                ]) !!}
                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection