@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Compartilhamento de joias
    </h2>
</legend>


@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="relatorio_compartilhar">
    <thead>
        <tr>
            <th>Data</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Joia (nome)</th>
            <th>Expositor (nome)</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($registro->created_at)) }}</td>
            <td>{{ $registro->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $registro->email }}
            </td>
            <td>{{ $registro->nome_joia }}</td>
            <td>{{ $registro->expositor_nome }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection