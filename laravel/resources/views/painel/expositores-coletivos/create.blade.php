@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Expositores - Coletivos | Galerias | Escolas /</small> Adicionar Expositor Coletivo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.expositores-coletivos.store', 'files' => true]) !!}

        @include('painel.expositores-coletivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
