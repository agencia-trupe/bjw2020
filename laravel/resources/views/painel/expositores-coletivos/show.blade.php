@extends('painel.common.template')

@section('content')

<legend>
    <h2>{{ $registro->nome }}</h2>
</legend>

<div class="form-group">
    <label>País</label>
    <div class="well">{{ $paises->find($registro->pais_id)->nome }}</div>
</div>

<div class="form-group">
    <label>Ano</label>
    <div class="well">{{ $registro->ano }}</div>
</div>

<div class="form-group">
    <label>Texto</label>
    <div class="well">{!! $registro->texto !!}</div>
</div>

<div class="form-group">
    <label>Whatsapp</label>
    <div class="well">{{ $registro->whatsapp }}</div>
</div>

<div class="form-group">
    <label>Instagram</label>
    <div class="well">{{ $registro->instagram }}</div>
</div>

<div class="form-group">
    <label>E-mail</label>
    <div class="well">{{ $registro->email }}</div>
</div>

<div class="form-group">
    <label>Website</label>
    <div class="well">{{ $registro->website }}</div>
</div>

<div class="form-group">
    <label>Capa</label>
    <div class="well"><img src="{{ asset('assets/img/expositores/'.$registro->capa) }}" alt=""></div>
</div>

<div class="form-group">
    <label>Vídeo</label>
    <div class="well">{{ $registro->video }}</div>
</div>

<a href="{{ route('painel.expositores-coletivos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop