@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.expositores-coletivos.index') }}" title="Voltar para Expositores Coletivos" class="btn btn-sm btn-default">
        &larr; Voltar para Expositores Coletivos    </a>

    <legend>
        <h2>
            <small>{{ $registro->nome }} / Imagens:</small> 

            {!! Form::open(['route' => ['painel.expositores-coletivos.imagens.store', $registro->id], 'files' => true, 'class' => 'pull-right']) !!}
            <div class="btn-group btn-group-sm pull-right">
                <span class="btn btn-success btn-sm" style="position:relative;overflow:hidden">
                    <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>
                    Adicionar Imagens
                    <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
                </span>
            </div>
            {!! Form::close() !!}
        </h2>
        <p class="alerta-quantidade">Podem ser adicionadas até 5 imagens</p>
    </legend>

    <div class="progress progress-striped active">
        <div class="progress-bar" style="width: 0"></div>
    </div>

    <div class="alert alert-block alert-danger errors" style="display:none"></div>

    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <span class="glyphicon glyphicon-move" style="margin-right: 10px;"></span>
            Clique e arraste as imagens para ordená-las.
        </small>
    </div>

    <div id="imagens" data-table="expositores_coletivos_imagens">
    @if(!count($imagens))
        <div class="alert alert-warning no-images" role="alert">Nenhuma imagem cadastrada.</div>
    @else
        @foreach($imagens as $imagem)
        @include('painel.expositores-coletivos.imagens.imagem')
        @endforeach
    @endif
    </div>

@endsection
