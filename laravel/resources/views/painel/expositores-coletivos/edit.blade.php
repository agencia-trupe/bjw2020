@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $registro->nome}} /</small> Editar Expositor Coletivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.expositores-coletivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.expositores-coletivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
