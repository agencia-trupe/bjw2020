@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="pais_id">País</label>
    <select name="pais_id" id="paises" class="form-control">
        <option value="" selected>Selecione</option>
        @foreach($paises as $pais)
        <option value="{{ $pais->id }}">{{ $pais->nome }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::text('ano', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto (INGLÊS)') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('whatsapp', 'Whatsapp (Exemplo: +55 11 9XXXX XXXX)') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
    <p class="alerta-quantidade">Inclur numero completo <strong>sem traços</strong>, incluindo: +código-do-país código-da-cidade telefone-completo <strong>(Exemplo: +55 11 9 9999 9999)</strong></p>
    <p class="alerta-quantidade"><strong>Veja as definições do whatsapp</strong><a href="https://faq.whatsapp.com/general/about-international-phone-number-format/?lang=pt_br" target="_blank"> AQUI</a></p>
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('website', 'Website (link)') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/expositores/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Video (link)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
    <p class="alerta-quantidade">Incluir somente o ID do vídeo (Exemplo: https://vimeo.com/<strong>218983866</strong>)</p>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.expositores-coletivos.index') }}" class="btn btn-default btn-voltar">Voltar</a>