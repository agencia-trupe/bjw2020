@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome do Evento') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('data_inicio', 'Data de Inicio do Evento') !!}
    {!! Form::date('data_inicio', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('data_fim', 'Data do Fim do Evento') !!}
    {!! Form::date('data_fim', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('realizacao', 'Realização') !!}
    {!! Form::text('realizacao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('site', 'Site do Evento') !!}
    {!! Form::text('site', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone (Exemplo: +55 11 9XXXX XXXX)') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('evento_duracao', 'Duração do evento') !!}
    {!! Form::text('evento_duracao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('evento_duracao_en', 'Duração do evento - INGLÊS') !!}
    {!! Form::text('evento_duracao_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_inicio', 'Frase sobre evento (início: "Joalheiros Contemporâneos..")') !!}
    {!! Form::textarea('frase_inicio', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_inicio_en', 'Frase sobre evento (início: "Joalheiros Contemporâneos..") - INGLÊS') !!}
    {!! Form::textarea('frase_inicio_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_agenda', 'Frase agenda ("Além de uma agenda..")') !!}
    {!! Form::text('frase_agenda', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_agenda_en', 'Frase agenda ("Além de uma agenda..") - INGLÊS') !!}
    {!! Form::text('frase_agenda_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_saudacao', 'Frase saudação ("Bom evento!")') !!}
    {!! Form::text('frase_saudacao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_saudacao_en', 'Frase saudação ("Bom evento!") - INGLÊS') !!}
    {!! Form::text('frase_saudacao_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('evento_periodo', 'Período do evento ("3 a 22 dez..")') !!}
    {!! Form::text('evento_periodo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('evento_periodo_en', 'Período do evento ("3 a 22 dez..") - INGLÊS') !!}
    {!! Form::text('evento_periodo_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_sobre', 'Frase ("20 dias, 24 horas..")') !!}
    {!! Form::textarea('frase_sobre', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_sobre_en', 'Frase ("20 dias, 24 horas..") - INGLÊS') !!}
    {!! Form::textarea('frase_sobre_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_sobre', 'Texto sobre o Evento') !!}
    {!! Form::textarea('texto_sobre', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_sobre_en', 'Texto sobre o Evento (INGLÊS)') !!}
    {!! Form::textarea('texto_sobre_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}