@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Evento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.evento.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.evento.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
