<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.evento*')) class="active" @endif>
        <a href="{{ route('painel.evento.index') }}">Evento</a>
    </li>
    <li @if(Tools::routeIs('painel.agenda*')) class="active" @endif>
        <a href="{{ route('painel.agenda.index') }}">Agenda</a>
    </li>
    <li @if(Tools::routeIs('painel.expositores-individuais*')) class="active" @endif>
        <a href="{{ route('painel.expositores-individuais.index') }}">Expositores Individuais</a>
    </li>
    <li @if(Tools::routeIs('painel.expositores-coletivos*')) class="active" @endif>
        <a href="{{ route('painel.expositores-coletivos.index') }}">Expositores Coletivos</a>
    </li>
    <li @if(Tools::routeIs('painel.assinaturas-livro*')) class="active" @endif>
        <a href="{{ route('painel.assinaturas-livro.index') }}">Assinaturas</a>
    </li>
    <li @if(Tools::routeIs('painel.cadastros*')) class="active" @endif>
        <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
    </li>
    <li class="dropdown @if(Tools::routeIs('painel.relatorios*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatórios <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.relatorios.download-ebook')) class="active" @endif>
                <a href="{{ route('painel.relatorios.download-ebook') }}">Download E-book</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.palestra-zoom')) class="active" @endif>
                <a href="{{ route('painel.relatorios.palestra-zoom') }}">Palestra Zoom</a>
            </li>
            <li @if(Tools::routeIs('painel.relatorios.compartilhar-joia')) class="active" @endif>
                <a href="{{ route('painel.relatorios.compartilhar-joia') }}">Compartilhar Joia</a>
            </li>
        </ul>
    </li>
</ul>