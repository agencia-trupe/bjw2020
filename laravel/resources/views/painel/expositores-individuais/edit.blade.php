@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $registro->nome }} /</small> Editar Expositor Individual</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.expositores-individuais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.expositores-individuais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
