@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        {{ $registro->nome }} | Processo
        @if(empty($processo))
        <a href="{{ route('painel.expositores-individuais.processo.create', $registro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Processo</a>
        @endif
    </h2>
    <p class="alerta-quantidade">Podem ser adicionadas até 3 imagens e 1 vídeo</p>
</legend>

@if(empty($processo))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

@else
<div class="processo">
    <h4 class="titulo">Vídeo:</h4>
    @if ($processo->video)
    <p class="paragrafo">{{ $processo->video }}</p>
    @else
    <p class="paragrafo">Sem vídeo</p>
    @endif

    <hr>

    <h4 class="titulo">Imagens:</h4>
    @if ($processo->imagem1)
    <img src="{{ asset('assets/img/expositores-individuais/processo/'.$processo->imagem1) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($processo->imagem2)
    <img src="{{ asset('assets/img/expositores-individuais/processo/'.$processo->imagem2) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($processo->imagem3)
    <img src="{{ asset('assets/img/expositores-individuais/processo/'.$processo->imagem3) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if(!$processo->imagem1 && !$processo->imagem2 && !$processo->imagem3)
    <p class="paragrafo">Sem imagens</p>
    <hr>
    @endif
</div>

{!! Form::open([
'route' => ['painel.expositores-individuais.processo.destroy', $registro->id, $processo->id],
'method' => 'delete'
]) !!}

<button type="submit" class="btn btn-danger btn-sm btn-delete" style="margin-right:10px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

<a href="{{ route('painel.expositores-individuais.processo.edit', [$registro->id, $processo->id] ) }}" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;">
    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
</a>

@endif

<a href="{{ route('painel.expositores-individuais.index') }}" class="btn btn-default btn-sm btn-voltar"><span class="glyphicon glyphicon-menu-left" style="margin-right:10px;"></span>Voltar</a>

@endsection