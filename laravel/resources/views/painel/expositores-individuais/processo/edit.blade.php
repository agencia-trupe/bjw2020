@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $registro->nome }} /</small> Editar Processo</h2>
    </legend>

    {!! Form::model($processo, [
        'route'  => ['painel.expositores-individuais.processo.update', $registro->id, $processo->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.expositores-individuais.processo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
