@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $registro->nome }} /</small> Adicionar Processo</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.expositores-individuais.processo.store', $registro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.expositores-individuais.processo.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection