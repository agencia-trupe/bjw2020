@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.expositores-individuais.index') }}" title="Voltar para Expositores" class="btn btn-sm btn-default">
    &larr; Voltar para Expositor </a>

<legend>
    <h2>
        {{ $registro->nome }} | Joias
        <a href="{{ route('painel.expositores-individuais.joias.create', $registro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Joia</a>
    </h2>
    <p class="alerta-quantidade">Podem ser adicionadas até 5 joias</p>
</legend>

@if(!count($joias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="expositores_individuais_joias">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Imagens</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($joias as $joia)
        <tr class="tr-row" id="{{ $joia->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $joia->nome_joia }}</td>
            <td>{!! $joia->descricao !!}</td>
            <td>
                @if ($joia->imagem1 || $joia->imagem2 || $joia->imagem3)
                <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem1) }}" class="imgs-tabelas" alt="">
                <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem2) }}" class="imgs-tabelas" alt="">
                <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem3) }}" class="imgs-tabelas" alt="">
                @else
                <p>Sem imagens</p>
                @endif
            </td>
            <td class="crud-actions" style="width:180px;">
                {!! Form::open([
                'route' => ['painel.expositores-individuais.joias.destroy', $registro->id, $joia->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.expositores-individuais.joias.edit', [$registro->id, $joia->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection