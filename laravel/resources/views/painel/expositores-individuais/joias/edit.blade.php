@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $registro->nome }} /</small> Editar Joia</h2>
    </legend>

    {!! Form::model($joia, [
        'route'  => ['painel.expositores-individuais.joias.update', $registro->id, $joia->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.expositores-individuais.joias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
