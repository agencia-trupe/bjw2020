@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome_joia', 'Nome da Jóia') !!}
    {!! Form::text('nome_joia', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_joia_en', 'Nome da Jóia (INGLÊS)') !!}
    {!! Form::text('nome_joia_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição da Jóia') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_en', 'Descrição da Jóia (INGLÊS)') !!}
    {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem1', 'Imagem 1 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/expositores-individuais/joias/'.$joia->imagem1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem2', 'Imagem 2 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/expositores-individuais/joias/'.$joia->imagem2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem2', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem3', 'Imagem 3 (Opcional)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/expositores-individuais/joias/'.$joia->imagem3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem3', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.expositores-individuais.joias.index', $registro->id) }}" class="btn btn-default btn-voltar">Voltar</a>
