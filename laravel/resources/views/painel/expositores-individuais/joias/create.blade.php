@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $registro->nome }} /</small> Adicionar Jóia</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.expositores-individuais.joias.store', $registro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.expositores-individuais.joias.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection