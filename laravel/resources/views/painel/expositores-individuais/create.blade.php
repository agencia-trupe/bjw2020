@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Expositores Individuais /</small> Adicionar Expositor Individual</h2>
    </legend>

    {!! Form::open(['route' => 'painel.expositores-individuais.store', 'files' => true]) !!}

        @include('painel.expositores-individuais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
