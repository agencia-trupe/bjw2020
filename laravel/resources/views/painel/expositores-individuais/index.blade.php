@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Expositores Individuais
        <a href="{{ route('painel.expositores-individuais.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Expositor Individual</a>
    </h2>
</legend>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="expositores_individuais">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Joias</th>
            <th>Processo</th>
            <th>Campanha</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ $registro->nome }}</td>
            <td><a href="{{ route('painel.expositores-individuais.joias.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Joias
                </a></td>
            <td><a href="{{ route('painel.expositores-individuais.processo.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Processo
                </a></td>
            <td><a href="{{ route('painel.expositores-individuais.campanha.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Campanha
                </a></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.expositores-individuais.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.expositores-individuais.show', $registro->id) }}" class="btn btn-success btn-sm pull-left">
                        <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Detalhes
                    </a>
                    <a href="{{ route('painel.expositores-individuais.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection