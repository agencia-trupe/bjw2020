@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $registro->nome }} /</small> Editar Campanha</h2>
    </legend>

    {!! Form::model($campanha, [
        'route'  => ['painel.expositores-individuais.campanha.update', $registro->id, $campanha->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.expositores-individuais.campanha.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
