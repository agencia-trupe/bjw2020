@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        {{ $registro->nome }} | Campanha
        @if(empty($campanha))
        <a href="{{ route('painel.expositores-individuais.campanha.create', $registro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Campanha</a>
        @endif
    </h2>
    <p class="alerta-quantidade">Podem ser adicionadas até 3 imagens e 2 vídeos</p>
</legend>

@if(empty($campanha))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

@else
<div class="processo">
    <h4 class="titulo">Vídeos:</h4>
    @if ($campanha->video1 || $campanha->video2)
    <p class="paragrafo">{{ $campanha->video1 }}</p>
    <p class="paragrafo">{{ $campanha->video2 }}</p>
    @else
    <p class="paragrafo">Sem vídeo</p>
    @endif

    <hr>

    <h4 class="titulo">Imagens:</h4>
    @if ($campanha->imagem1)
    <img src="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem1) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($campanha->imagem2)
    <img src="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem2) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if ($campanha->imagem3)
    <img src="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem3) }}" alt="" class="processo-img">
    <hr>
    @endif

    @if(!$campanha->imagem1 && !$campanha->imagem2 && !$campanha->imagem3)
    <p class="paragrafo">Sem imagens</p>
    <hr>
    @endif
</div>

{!! Form::open([
'route' => ['painel.expositores-individuais.campanha.destroy', $registro->id, $campanha->id],
'method' => 'delete'
]) !!}

<button type="submit" class="btn btn-danger btn-sm btn-delete" style="margin-right:10px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

<a href="{{ route('painel.expositores-individuais.campanha.edit', [$registro->id, $campanha->id] ) }}" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;">
    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
</a>

@endif

<a href="{{ route('painel.expositores-individuais.index') }}" class="btn btn-default btn-sm btn-voltar"><span class="glyphicon glyphicon-menu-left" style="margin-right:10px;"></span>Voltar</a>

@endsection