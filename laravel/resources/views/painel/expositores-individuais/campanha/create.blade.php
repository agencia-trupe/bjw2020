@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $registro->nome }} /</small> Adicionar Campanha</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.expositores-individuais.campanha.store', $registro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.expositores-individuais.campanha.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection