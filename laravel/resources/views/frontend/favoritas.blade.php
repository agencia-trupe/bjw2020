@extends('frontend.common.template')

@section('content')

<div class="pag-favoritas">
    <h2 class="titulo">{{ trans('frontend.favoritas.titulo') }}</h2>
    <p class="user-nome">{{ $userLogado->nome }}</p>

    <div class="joias-favoritas">
        @foreach($itens as $joias)
        @foreach($joias as $joia)
        @if ($joia != null)
        <div class="joia">
            <div class="imgs-joias">
                <div class="img">
                    <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem1) }}" alt="" class="variacao">
                </div>
                <div class="img">
                    <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem2) }}" alt="" class="variacao">
                </div>
                <div class="img">
                    <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem3) }}" alt="" class="variacao">
                </div>
            </div>
            <p class="nome-joia">{{ $joia->{trans('banco.nome_joia')} }}</p>
            <div class="descricao-joia">{!! $joia->{trans('banco.descricao')} !!}</div>
            <div class="acoes">
                {!! Form::open([
                'route' => ['favoritas.delete', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                'method' => 'delete'
                ]) !!}
                <button type="submit" class="favoritar-joia">{{ trans('frontend.favoritas.exluir') }} <img src="{{ asset('assets/img/layout/ico-desfavoritar.svg') }}" alt=""></button>
                <!-- <a href="" class="favoritar-joia">favoritar joia <img src="{{ asset('assets/img/layout/ico-favoritar.svg') }}" alt=""></a> -->
                {!! Form::close() !!}
                <button class="compartilhar-joia" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar">{{ trans('frontend.favoritas.compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></button>
                <!-- <a href="" class="compartilhar-joia">compartilhar joia <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></a> -->
            </div>
        </div>
        @endif
        @endforeach
        @endforeach
    </div>

    <!-- MODAL COMPARTILHAR -->
    <div class="modal fade" id="modalCompartilhar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.expositor.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
                <ul class="compartilhamento">
                    @if(auth('evento')->check())
                    <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp2.svg') }}" alt=""></a>
                    <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                    <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email2.svg') }}" alt=""></a>
                    @else
                    <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp2.svg') }}" alt=""></a>
                    <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                    <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email2.svg') }}" alt=""></a>
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <hr class="hr-atualizar-cadastro">

    <div class="atualizar-cadastro">
        <h2 class="titulo">{{ trans('frontend.favoritas.atualizar-cadastro') }}</h2>
        <form action="{{ route('evento.atualizar.user', $userLogado->id) }}" method="POST">
            {!! csrf_field() !!}
            <label for="email">LOGIN (E-MAIL)</label>
            <input type="email" name="email" value="{{ $userLogado->email }}" required>
            <label for="senha">{{ trans('frontend.modal.senha') }}</label>
            <input type="password" name="senha" value="{{ $userLogado->senha }}" required>
            <label for="senha_confirmation">{{ trans('frontend.modal.repetir-senha') }}</label>
            <input type="password" name="senha_confirmation" id="senha_confirmation" required>
            <label for="nome">{{ trans('frontend.modal.nome') }}</label>
            <input type="text" name="nome" value="{{ $userLogado->nome }}" required>
            <label for="telefone">{{ trans('frontend.modal.telefone') }}</label>
            <input type="text" name="telefone" value="{{ $userLogado->telefone }}" required>
            <label for="pais">{{ trans('frontend.modal.pais') }}</label>
            <input type="text" name="pais" value="{{ $userLogado->pais }}" required>
            <button type="submit" class="btn btn-primary" id="btnAtualizar">{{ trans('frontend.favoritas.btn-atualizar') }}</button>
        </form>
    </div>
</div>

<div class="assine-livro">
    @if(auth('evento')->check())
    <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-assine-livro">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
    @else
    <a href="" class="btn-assine-livro link-deslogado">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
    @endif
</div>
@endsection