@extends('frontend.common.template')

@section('content')

<div class="expositor-coletivo">
    <p class="expo-nome">{{ $coletivo->nome }}</p>
    <p class="expo-pais">{{ $paises->find($coletivo->pais_id)->{trans('banco.nome')} }} ∙ {{ $coletivo->ano }}</p>

    <div class="expo-texto">{!! $coletivo->{trans('banco.texto')} !!}</div>

    @php $formatoWhats = str_replace(" ", "", $coletivo->whatsapp) @endphp
    <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="telefone" target="_blank">
        <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt=""> {{ trans('frontend.expositor.conversa-coletivo') }}</a>

    @foreach($imagens as $img)
    @if ($img != null)
    <img src="{{ asset('assets/img/expositores-coletivos/imagens/'.$img->imagem) }}" alt="" class="coletivo-img">
    @endif
    @endforeach

    @if($coletivo->video)
    <iframe src="https://www.youtube.com/embed/{{ $coletivo->video }}" width="760" height="430" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    @endif

    @php $formatoWhats = str_replace(" ", "", $coletivo->whatsapp) @endphp
    <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="telefone" target="_blank">
        <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt=""> {{ trans('frontend.expositor.conversa-coletivo') }}</a>

    <div class="links">
        @if(auth('evento')->check())
        <a href="{{ $coletivo->instagram }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt=""> {{ trans('frontend.expositor.instagram') }}</a>
        <a href="mailto:{{ $coletivo->email }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-email.svg') }}" alt=""> {{ trans('frontend.expositor.email') }}</a>
        @if($coletivo->website != null)
        <a href="{{ $coletivo->website }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-website.svg') }}" alt=""> {{ trans('frontend.expositor.website') }}</a>
        @endif
        @else
        <a href="{{ $coletivo->instagram }}" class="link-deslogado" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt=""> {{ trans('frontend.expositor.instagram') }}</a>
        <a href="mailto:{{ $coletivo->email }}" class="link-deslogado" target="_blank"><img src="{{ asset('assets/img/layout/ico-email.svg') }}" alt=""> {{ trans('frontend.expositor.email') }}</a>
        @if($coletivo->website != null)
        <a href="{{ $coletivo->website }}" class="link-deslogado" target="_blank"><img src="{{ asset('assets/img/layout/ico-website.svg') }}" alt=""> {{ trans('frontend.expositor.website') }}</a>
        @endif
        @endif
    </div>

</div>
<div class="filtro">
    <div class="centralizado">
        <h2 class="titulo">{{ trans('frontend.filtro.expositores-titulo') }}</h2>
        <!-- FILTRO -->
        <div class="filtro-expositores">
            <select name="pais_id" id="paises">
                <option value="" selected>{{ trans('frontend.filtro.filtro-pais') }}</option>
                @foreach($paises as $pais)
                <option value="{{ $pais->id }}">{{ $pais->nome }}</option>
                @endforeach
            </select>
            <select name="individual_id" id="IndividualPais">
                <option value="" selected>{{ trans('frontend.filtro.filtro-individuais') }}</option>
            </select>
            <select name="coletivo_id" id="ColetivoPais">
                <option value="" selected>{{ trans('frontend.filtro.filtro-coletivos') }}</option>
            </select>
            <a href="#" class="lupa-filtro" id="lupaGeral"></a>
        </div>
    </div>
</div>

<div class="assine-livro">
    @if(auth('evento')->check())
    <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-assine-livro">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
    @else
    <a href="" class="btn-assine-livro link-deslogado">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
    @endif
</div>
@endsection