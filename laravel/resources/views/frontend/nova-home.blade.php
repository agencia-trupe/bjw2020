@extends('frontend.common.template')

@section('content')

<div class="nova-home">
    <div class="nome-evento">BRAZIL JEWELRY WEEK</div>
    <img src="{{ asset('assets/img/layout/mapa-americalatina.svg') }}" alt="" class="mapa-evento">
    <p class="evento-encerrado">{{ trans('frontend.home.evento-encerrado') }}</p>
</div>

@endsection