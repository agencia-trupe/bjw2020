@extends('frontend.common.template')

@section('content')

<div class="filtro-busca">
    <h2 class="titulo">{{ trans('frontend.filtro.expositores-titulo') }}</h2>
    <!-- FILTRO -->
    <div class="filtro-expositores">
        <select name="pais_id" id="paises">
            <option value="" selected>{{ trans('frontend.filtro.filtro-pais') }}</option>
            @foreach($paises as $pais)
            <option value="{{ $pais->id }}">{{ $pais->{trans('banco.nome')} }}</option>
            @endforeach
        </select>
        <select name="individual_id" id="IndividualPais">
            <option value="" selected>{{ trans('frontend.filtro.filtro-individuais') }}</option>
        </select>
        <select name="coletivo_id" id="ColetivoPais">
            <option value="" selected>{{ trans('frontend.filtro.filtro-coletivos') }}</option>
        </select>
        <a href="#" class="lupa-filtro" id="lupaFiltro"></a>
    </div>
</div>

<div class="resultados">
    <div class="todos-expositores" id="todosExpositores">
        <!-- JQUERY-AJAX (ver main.js) -->
    </div>
</div>

<div class="assine-livro">
    @if(auth('evento')->check())
    <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-assine-livro">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
    @else
    <a href="" class="btn-assine-livro link-deslogado">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
    @endif
</div>

@endsection