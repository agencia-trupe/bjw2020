@extends('frontend.common.template')

@section('content')

<div class="expositor-individual">
    <p class="expo-nome">{{ $individual->nome }}</p>
    <p class="expo-pais">{{ $paises->find($individual->pais_id)->{trans('banco.nome')} }}</p>

    <div class="loading">
        <img src="{{ asset('assets/img/layout/load.gif') }}" alt="">
    </div>
    <div class="joias">
        @foreach($joias as $joia)
        @if ($joia != null)
        <div class="joia">
            <div class="imgs-joias">
                @if($joia->imagem1 != null)
                <div class="img">
                    <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem1) }}" alt="" class="variacao">
                </div>
                @endif
                @if($joia->imagem2 != null)
                <div class="img">
                    <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem2) }}" alt="" class="variacao">
                </div>
                @endif
                @if($joia->imagem3 != null)
                <div class="img">
                    <img src="{{ asset('assets/img/expositores-individuais/joias/'.$joia->imagem3) }}" alt="" class="variacao">
                </div>
                @endif
            </div>
            <p class="nome-joia">{{ $joia->{trans('banco.nome_joia')} }}</p>
            <div class="descricao-joia">{!! $joia->{trans('banco.descricao')} !!}</div>
            <div class="acoes">
                @if(auth('evento')->check())
                {!! Form::open([
                'route' => ['favoritas.post', 'user' => auth('evento')->user()->id, 'joia' => $joia->id],
                'method' => 'post'
                ]) !!}
                <button type="submit" class="favoritar-joia">{{ trans('frontend.expositor.favoritar-joia') }} <img src="{{ asset('assets/img/layout/ico-favoritar.svg') }}" alt=""></button>
                {!! Form::close() !!}
                <button class="compartilhar-joia" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar">{{ trans('frontend.expositor.compartilhar-joia') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></button>
                @else
                <button type="submit" class="favoritar-joia link-deslogado">{{ trans('frontend.expositor.favoritar-joia') }} <img src="{{ asset('assets/img/layout/ico-favoritar.svg') }}" alt=""></button>
                <button class="compartilhar-joia link-deslogado" ype="button" id="btnModalCompartilhar" data-toggle="modal" data-target="#modalCompartilhar">{{ trans('frontend.expositor.compartilhar-joia') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></button>
                @endif
            </div>
        </div>
        @endif
        @endforeach
    </div>

    <div class="dados-expositor">
        <img src="{{ asset('assets/img/expositores/fotos/'.$individual->foto) }}" alt="" class="foto-expositor">
        <div class="textos">
            <div class="sobre-expositor">{!! $individual->{trans('banco.texto')} !!}</div>
            @if($individual->whatsapp != null || $individual->whatsapp != "")
            @php $formatoWhats = str_replace(" ", "", $individual->whatsapp) @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="telefone" target="_blank">
                <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt=""> {{ trans('frontend.expositor.conversa-joalheiro') }}</a>
            @endif
        </div>
    </div>

    @if ($processo != null)
    <div class="processo">
        <iframe src="https://www.youtube.com/embed/{{ $processo->video }}" width="760" height="430" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        <div class="imgs-processo">
            @if($processo->imagem1 != null)
            <img src="{{ asset('assets/img/expositores-individuais/processo/'.$processo->imagem1) }}" alt="">
            @endif
            @if($processo->imagem2 != null)
            <img src="{{ asset('assets/img/expositores-individuais/processo/'.$processo->imagem2) }}" alt="">
            @endif
            @if($processo->imagem3 != null)
            <img src="{{ asset('assets/img/expositores-individuais/processo/'.$processo->imagem3) }}" alt="">
            @endif
        </div>
    </div>
    @endif

    @if ($campanha != null)
    <div class="campanha">
        @if($campanha->video1 != null || $campanha->video1 != "")
        <iframe src="https://www.youtube.com/embed/{{ $campanha->video1 }}" width="760" height="430" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        @endif
        @if($campanha->video2 != null || $campanha->video2 != "")
        <iframe src="https://www.youtube.com/embed/{{ $campanha->video2 }}" width="760" height="430" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        @endif
        <div class="imgs-campanha">
            @if($campanha->imagem1 != null)
            <a href="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem1) }}" class="variacao">
                <img src="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem1) }}">
            </a>
            @endif
            @if($campanha->imagem2 != null)
            <a href="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem2) }}" class="variacao variacao-ativo" id="variacaoMaior">
                <img src="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem2) }}" alt="">
            </a>
            @endif
            @if($campanha->imagem3 != null)
            <a href="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem3) }}" class="variacao">
                <img src="{{ asset('assets/img/expositores-individuais/campanha/'.$campanha->imagem3) }}" alt="">
            </a>
            @endif
        </div>
    </div>
    @endif

    <div class="contatos-expositor">
        @if($individual->whatsapp != null || $individual->whatsapp != "")
        @php $formatoWhats = str_replace(" ", "", $individual->whatsapp) @endphp
        <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="telefone" target="_blank">
            <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt=""> {{ trans('frontend.expositor.conversa-joalheiro') }}</a>
        @endif
        <div class="links">
            @if(auth('evento')->check())
            <a href="{{ $individual->instagram }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt=""> {{ trans('frontend.expositor.instagram') }}</a>
            <a href="mailto:{{ $individual->email }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-email.svg') }}" alt=""> {{ trans('frontend.expositor.email') }}</a>
            @if($individual->website != null)
            <a href="{{ $individual->website }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-website.svg') }}" alt=""> {{ trans('frontend.expositor.website') }}</a>
            @endif
            @else
            <a href="{{ $individual->instagram }}" class="link-deslogado" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt=""> {{ trans('frontend.expositor.instagram') }}</a>
            <a href="mailto:{{ $individual->email }}" class="link-deslogado" target="_blank"><img src="{{ asset('assets/img/layout/ico-email.svg') }}" alt=""> {{ trans('frontend.expositor.email') }}</a>
            @if($individual->website != null)
            <a href="{{ $individual->website }}" class="link-deslogado" target="_blank"><img src="{{ asset('assets/img/layout/ico-website.svg') }}" alt=""> {{ trans('frontend.expositor.website') }}</a>
            @endif
            @endif
        </div>
    </div>

    <div class="filtro">
        <div class="centralizado">
            <h2 class="titulo">{{ trans('frontend.filtro.expositores-titulo') }}</h2>
            <!-- FILTRO -->
            <div class="filtro-expositores">
                <select name="pais_id" id="paises">
                    <option value="" selected>{{ trans('frontend.filtro.filtro-pais') }}</option>
                    @foreach($paises as $pais)
                    <option value="{{ $pais->id }}">{{ $pais->{trans('banco.nome')} }}</option>
                    @endforeach
                </select>
                <select name="individual_id" id="IndividualPais">
                    <option value="" selected>{{ trans('frontend.filtro.filtro-individuais') }}</option>
                </select>
                <select name="coletivo_id" id="ColetivoPais">
                    <option value="" selected>{{ trans('frontend.filtro.filtro-coletivos') }}</option>
                </select>
                <a href="#" class="lupa-filtro" id="lupaGeral"></a>
            </div>
        </div>
    </div>

    <div class="assine-livro">
        @if(auth('evento')->check())
        <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-assine-livro">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
        @else
        <a href="" class="btn-assine-livro link-deslogado">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
        @endif
    </div>
</div>

@endsection

<!-- MODAL COMPARTILHAR -->
<div class="modal fade" id="modalCompartilhar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.expositor.modal-compartilhar') }} <img src="{{ asset('assets/img/layout/ico-compartilhar.svg') }}" alt=""></h4>
            <ul class="compartilhamento">
                @if(auth('evento')->check())
                <a class="comp-whatsapp" href="{{ route('relatorio.whatsapp.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp2.svg') }}" alt=""></a>
                <a class="comp-facebook" href="{{ route('relatorio.facebook.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a class="comp-email" href="{{ route('relatorio.email.compartilhar', ['user' => auth('evento')->user()->id, 'joia' => $joia->id]) }}" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email2.svg') }}" alt=""></a>
                @else
                <a class="comp-whatsapp link-deslogado" href="" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp2.svg') }}" alt=""></a>
                <a class="comp-facebook link-deslogado" href="" title="Facebook" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a class="comp-email link-deslogado" href="" target="_blank" title="E-mail"><img src="{{ asset('assets/img/layout/ico-email2.svg') }}" alt=""></a>
                @endif
            </ul>
        </div>
    </div>
</div>