<div class="itens-menu">
    @unless(Tools::routeIs('novahome'))
    <div class="links">
        @if(auth('evento')->check())
        <p class="logado-nome">{{ trans('frontend.header.ola') }} {{ auth('evento')->user()->nome }}!</p>
        <a href="{{ route('favoritas', auth('evento')->user()->id) }}" class="logado-favoritas"> | {{ trans('frontend.header.pecas-favoritas') }} |</a>
        <a href="{{ route('evento.logout') }}" class="logado-logout">| LOGOUT |</a>
        @else
        <button type="button" id="btnModalLogin" data-toggle="modal" data-target="#modalLogin">| LOGIN |</button>
        @endif
    </div>
    @endunless
    <div class="idiomas">
        <a class="idiomas-titulo">{{ trans('frontend.header.idioma') }} <img src="{{ asset('assets/img/layout/seta-selecao-combobox.svg') }}" alt=""></a>
        <div class="idiomas-links" style="display: none;">
            @foreach([
            'pt' => 'Português',
            'en' => 'English',
            ] as $key => $title)
            @unless(app()->getLocale() == $key)
            <a href="{{ route('lang', $key) }}" class="idioma-troca">{{ $title }}</a>
            @endunless
            @endforeach
        </div>
    </div>
</div>