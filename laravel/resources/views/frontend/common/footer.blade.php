<footer>
    <div class="footer">
        <div class="footer-evento">
            <div class="evento-nav">
                <a href="{{ route('home') }}" class="nav-link">{{ trans('frontend.footer.link-home') }}</a>
                <a href="{{ route('agenda') }}" class="nav-link">{{ trans('frontend.footer.link-agenda') }}</a>
                <a href="{{ route('filtro', 0) }}" class="nav-link">{{ trans('frontend.footer.link-filtro') }}</a>
            </div>
            <a href="{{ route('home') }}" class="evento-nome">{{ $evento->nome }}</a>
            <div class="evento-dados">
                <a href="https://www.nucleojoalheria.org/" class="nav-dados" target="_blank">∙ {{ trans('frontend.footer.realizacao') }}: {{ $evento->realizacao }}</a>
                <a href="https://www.braziljewelryweek.com/" class="nav-site" target="_blank">∙ {{ $evento->site }}</a>
                @php $formatoWhats = str_replace(" ", "", $evento->telefone) @endphp
                <a href="https://api.whatsapp.com/send?phone={{ $formatoWhats }}" class="telefone" target="_blank">{{ $evento->telefone }}</a>
            </div>
        </div>

        <div class="patrocinadores">
            <p class="titulo">{{ trans('frontend.footer.patrocinadores') }}</p>
            <div class="todos">
                <div class="patrocinador"><img src="{{ asset('assets/img/layout/patrocinio3.jpg') }}" alt=""></div>
                <div class="patrocinador"><img src="{{ asset('assets/img/layout/patrocinio2.jpg') }}" alt=""></div>
                <div class="patrocinador"><img src="{{ asset('assets/img/layout/patrocinio1.jpg') }}" alt=""></div>
                <div class="patrocinador"><img src="{{ asset('assets/img/layout/patrocinio4.jpg') }}" alt=""></div>
                <div class="patrocinador"><img src="{{ asset('assets/img/layout/patrocinio5.jpg') }}" alt=""></div>
            </div>
        </div>

        <div class="direitos">
            <p class="dados-direitos"><a href="">{{ trans('frontend.footer.politica-privacidade') }}</a> ∙ © {{ date('Y') }} {{ $evento->nome }} | {{ trans('frontend.footer.direitos') }}</p>
        </div>
    </div>
</footer>