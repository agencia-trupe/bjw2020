@extends('frontend.common.template')

@section('content')

<div class="agenda">
    <h2 class="agenda-titulo">{{ trans('frontend.agenda.titulo') }}</h2>

    @php
    $count = 0;
    $white = true;
    @endphp

    @foreach($agendas as $agenda)

    @if($count == 0)
    @if($white)
    <div class="agenda-geral agenda-white">
        <div class="centralizado">
            <p class="dia-evento">{{ $agenda->{trans('banco.dia_evento')} }}</p>
            <p class="data-evento">{{ strftime("%d %B %Y", strtotime($agenda->data)) }}</p>
            @include('frontend.palestras', ['agenda' => $agenda->id])
        </div>

        @else
        <div class="agenda-geral agenda-gray">
            <div class="centralizado">
                <p class="dia-evento">{{ $agenda->{trans('banco.dia_evento')} }}</p>
                <p class="data-evento">{{ strftime("%d %B %Y", strtotime($agenda->data)) }}</p>
                @include('frontend.palestras', ['agenda' => $agenda->id])
            </div>

            @endif
            @endif

            @php
            $count++;
            @endphp
            @if($count == 1)
        </div>

        @php
        $count = 0;
        $white = !$white;
        @endphp

        @endif

        @endforeach
    </div>

    <button class="btn-agenda-mais">{{ trans('frontend.agenda.ver-mais') }} +</button>

    <div class="filtro">
        <div class="centralizado">
            <h2 class="titulo">{{ trans('frontend.filtro.expositores-titulo') }}</h2>
            <!-- FILTRO -->
            <div class="filtro-expositores">
                <select name="pais_id" id="paises">
                    <option value="" selected>{{ trans('frontend.filtro.filtro-pais') }}</option>
                    @foreach($paises as $pais)
                    <option value="{{ $pais->id }}">{{ $pais->{trans('banco.nome')} }}</option>
                    @endforeach
                </select>
                <select name="individual_id" id="IndividualPais">
                    <option value="" selected>{{ trans('frontend.filtro.filtro-individuais') }}</option>
                </select>
                <select name="coletivo_id" id="ColetivoPais">
                    <option value="" selected>{{ trans('frontend.filtro.filtro-coletivos') }}</option>
                </select>
                <a href="#" class="lupa-filtro" id="lupaGeral"></a>
            </div>
        </div>
    </div>

    <div class="assine-livro">
        @if(auth('evento')->check())
        <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-assine-livro">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
        @else
        <a href="" class="btn-assine-livro link-deslogado">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
        @endif
    </div>

</div>

@endsection