@extends('frontend.common.template')

@section('content')

<div class="pag-assine-livro">
    <h2 class="titulo">{{ trans('frontend.assine-livro.titulo') }}</h2>

    <form action="{{ route('assine-livro.post', $user->id) }}" method="POST">
        <h4 class="deixe-mensagem">{{ trans('frontend.assine-livro.deixe-mensagem') }}</h4>
        {!! csrf_field() !!}
        <input type="hidden" name="user" class="user-cadastro" value="{{ $user->id }}">
        <textarea name="mensagem" required>{{ old('mensagem') }}</textarea>
        <div class="assinatura-enviada" style="display:none;">{{ trans('frontend.assine-livro.obrigado') }}</div>
        <p class="frase-aviso">{{ trans('frontend.assine-livro.frase-aviso') }}</p>
        <button type="submit" class="btn btn-primary" id="btnAssinarLivro">{{ trans('frontend.assine-livro.btn-assinar') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></button>
    </form>
</div>

<hr class="linha-assinaturas">

<div class="assinaturas masonry-grid">
    @foreach($assinaturas as $assinatura)
    <div class="assinatura grid-item">
        <div class="mensagem">{!! $assinatura->mensagem !!}</div>
        <p class="nome">{{ $assinatura->nome }} • {{ $assinatura->pais }}</p>
        <p class="data-assinatura">{{ strftime("%d %B %Y", strtotime($assinatura->created_at)) }}</p>
    </div>
    @endforeach
</div>
<button class="btn-assinaturas-mais">{{ trans('frontend.assine-livro.ver-mais') }} +</button>

@endsection