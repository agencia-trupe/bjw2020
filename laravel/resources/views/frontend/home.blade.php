@extends('frontend.common.template')

@section('content')

<div class="home">
    <div class="dados-evento">
        <div class="nome-evento">{{ $evento->nome }}</div>
        <img src="{{ asset('assets/img/layout/mapa-americalatina.svg') }}" alt="" class="mapa-evento">
        <p hidden class="data-fim">{{ $evento->data_fim }}</p>
        <div class="cronometro-evento">{{ trans('frontend.home.cronometro-faltam') }}
            <p class="x-dias"></p> {{ trans('frontend.home.cronometro-dias') }}
            <p class="x-horas"></p> {{ trans('frontend.home.cronometro-horas') }}
            <p class="x-minutos"></p> {{ trans('frontend.home.cronometro-minutos-aofim') }}
        </div>
        <p class="duracao-evento">{{ $evento->{trans('banco.evento_duracao')} }}</p>
        <p class="texto-evento">{!! $evento->{trans('banco.frase_inicio')} !!}</p>
        <p class="frase-evento">{{ $evento->{trans('banco.frase_agenda')} }}</p>
        <p class="bom-evento">{{ $evento->{trans('banco.frase_saudacao')} }}</p>
    </div>
    <div class="expositores">
        <h2 class="titulo">{{ trans('frontend.filtro.expositores-titulo') }}</h2>
        <!-- FILTRO -->
        <div class="filtro-expositores">
            <select name="pais_id" id="paises">
                <option value="" selected>{{ trans('frontend.filtro.filtro-pais') }}</option>
                @foreach($paises as $pais)
                <option value="{{ $pais->id }}">{{ $pais->{trans('banco.nome')} }}</option>
                @endforeach
            </select>
            <select name="individual_id" id="IndividualPais">
                <option value="" selected>{{ trans('frontend.filtro.filtro-individuais') }}</option>
            </select>
            <select name="coletivo_id" id="ColetivoPais">
                <option value="" selected>{{ trans('frontend.filtro.filtro-coletivos') }}</option>
            </select>
            <a href="#" class="lupa-filtro" id="lupaHome"></a>
        </div>
        <!-- EXPOSITORES (TODOS)-->
        <div class="todos-expositores" id="todosExpositores">
            <!-- JQUERY-AJAX (ver main.js) -->
        </div>
    </div>
    <div class="agenda">
        <h2 class="titulo">{{ trans('frontend.home.agenda-titulo') }}</h2>
        <a href="{{ route('agenda') }}" class="btn-agenda-completa">{{ trans('frontend.home.agenda-completa') }} <img src="{{ asset('assets/img/layout/seta-avancar.svg') }}" alt=""></a>
        <!-- PRIMEIRA DATA A PARTIR DE HOJE (date) -->
        @if ($agenda != null)
        <p class="dia-evento">{{ $agenda->{trans('banco.dia_evento')} }}</p>
        <p class="data-evento">{{ strftime("%d %B %Y", strtotime($agenda->data)) }}</p>

        <div class="palestras">
            <div class="fio-vertical"></div>
            @php
            $count = 0;
            $right = true;
            @endphp

            @foreach($palestras as $palestra)
            @if($count == 0)
            @if($right)
            <div class="palestra palestra-right">
                <div class="horario">{{ $palestra->horario }}
                    <p class="tipo-hora">{{ trans('frontend.agenda.horario-brasil') }}</p>
                </div>
                <div class="dados">
                    <p class="tipo">{{ $palestra->{trans('banco.tipo')} }}</p>
                    <p class="titulo">{{ $palestra->{trans('banco.titulo')} }}</p>
                    <p class="nome">{{ $palestra->{trans('banco.nome')} }}</p>
                    <div class="descricao">{!! $palestra->{trans('banco.descricao')} !!}</div>
                    
                    @if(auth('evento')->check())
                    <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="link" target="_blank">{{ trans('frontend.agenda.transmissao') }}: <strong>Zoom - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                    <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="id-reuniao" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }} - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                    
                    @else
                    <a href="" class="link link-deslogado" target="_blank">{{ trans('frontend.agenda.transmissao') }}: <strong>Zoom - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                    <a href="" class="id-reuniao link-deslogado" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }} - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                    @endif
                    
                    <p class="senha-acesso">{{ trans('frontend.agenda.senha') }}: <strong>{{ $palestra->senha_acesso }}</strong></p>
                </div>
                <img src="{{ asset('assets/img/agenda/'.$palestra->imagem) }}" alt="" class="img-agenda">
                @else
                <div class="palestra palestra-left">
                    <div class="horario">{{ $palestra->horario }}
                        <p class="tipo-hora">{{ trans('frontend.agenda.horario-brasil') }}</p>
                    </div>
                    <div class="dados">
                        <p class="tipo">{{ $palestra->{trans('banco.tipo')} }}</p>
                        <p class="titulo">{{ $palestra->{trans('banco.titulo')} }}</p>
                        <p class="nome">{{ $palestra->{trans('banco.nome')} }}</p>
                        <div class="descricao">{!! $palestra->{trans('banco.descricao')} !!}</div>
                        
                        @if(auth('evento')->check())
                        <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="link" target="_blank">{{ trans('frontend.agenda.transmissao') }}: <strong>Zoom - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                        <a href="{{ route('relatorio.zoom', ['user' => auth('evento')->user()->id, 'palestra' => $palestra->id]) }}" class="id-reuniao" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }} - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                        
                        @else
                        <a href="" class="link link-deslogado" target="_blank">{{ trans('frontend.agenda.transmissao') }}: <strong>Zoom - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                        <a href="" class="id-reuniao link-deslogado" target="_blank">{{ trans('frontend.agenda.id-reuniao') }}: <strong>{{ $palestra->id_reuniao }} - {{ trans('frontend.agenda.clique') }}</strong><img src="{{ asset('assets/img/layout/seta-link-zoom.svg') }}" alt="" class="setinha"></a>
                        @endif
                        
                        <p class="senha-acesso">{{ trans('frontend.agenda.senha') }}: <strong>{{ $palestra->senha_acesso }}</strong></p>
                    </div>
                    <img src="{{ asset('assets/img/agenda/'.$palestra->imagem) }}" alt="" class="img-agenda">
                    @endif
                    @endif
                    @php
                    $count++;
                    @endphp
                    @if($count == 1)
                </div>
                @php
                $count = 0;
                $right = !$right;
                @endphp
                @endif
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="sobre-evento">
        <div class="centralizado">
            <div class="nome-evento">{{ $evento->nome }}</div>
            <p class="duracao">{{ $evento->{trans('banco.evento_periodo')} }}</p>
            <div class="texto">{!! $evento->{trans('banco.texto_sobre')} !!}</div>
            <div class="frase">{{ $evento->{trans('banco.frase_sobre')} }}</div>
            <div class="cronometro-evento">{{ trans('frontend.home.cronometro-faltam') }}
                <p class="x-dias"></p> {{ trans('frontend.home.cronometro-dias') }}
                <p class="x-horas"></p> {{ trans('frontend.home.cronometro-horas') }}
                <p class="x-minutos"></p> {{ trans('frontend.home.cronometro-minutos-aofim') }}
            </div>
            @if(auth('evento')->check())
            <a href="{{ route('ebook.download', auth('evento')->user()->id) }}" class="btn-ebook">{{ trans('frontend.home.obtenha-ebook') }} <img src="{{ asset('assets/img/layout/ico-ebook.svg') }}" alt=""></a>
            @else
            <a href="" class="btn-ebook link-deslogado">{{ trans('frontend.home.obtenha-ebook') }} <img src="{{ asset('assets/img/layout/ico-ebook.svg') }}" alt=""></a>
            @endif
        </div>
    </div>
    <div class="assine-livro">
        @if(auth('evento')->check())
        <a href="{{ route('assine-livro', auth('evento')->user()->id) }}" class="btn-assine-livro">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
        @else
        <a href="" class="btn-assine-livro link-deslogado">{{ trans('frontend.home.assine-livro') }} <img src="{{ asset('assets/img/layout/ico-assine-livro.svg') }}" alt=""></a>
        @endif
    </div>
</div>

@endsection