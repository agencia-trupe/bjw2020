<!DOCTYPE html>
<html>
<head>
    <title>{{ trans('frontend.reset.recuperacao') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        {{ $user->nome }}<br>
        {{ trans('frontend.reset.frase') }}:<br>
        <a href="{{ route('evento.password-reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">{{ trans('frontend.reset.clique-aqui') }}</a>
    </p>
</body>
</html>
