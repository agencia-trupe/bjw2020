@extends('frontend.common.template')

@section('content')
<div class="container password-reset">
    <div class="row" style="display: flex; justify-content: center;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-family: 'Raleway'; font-size: 24px; color: #A8A8A8; margin: 40px 0;">{{ trans('frontend.reset.titulo') }}</div>

                <div class="panel-body" style="width: 100%; display: flex; justify-content: center;">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('evento.password-reset.post') }}" style="width: 100%;">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="width: 100%; text-align: center;">
                            <label for="email" class="col-md-4 control-label">E-MAIL</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" style="width: 100%; border: 1px solid #A8A8A8; height: 40px; border-radius: 30px; padding: 0 15px; margin-bottom: 25px; outline: none;">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="width: 100%; text-align: center;">
                            <label for="password" class="col-md-4 control-label">{{ trans('frontend.modal.senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" style="width: 100%; border: 1px solid #A8A8A8; height: 40px; border-radius: 30px; padding: 0 15px; margin-bottom: 25px; outline: none;">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" style="width: 100%; text-align: center;">
                            <label for="password-confirm" class="col-md-4 control-label">{{ trans('frontend.modal.repetir-senha') }}</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" style="width: 100%; border: 1px solid #A8A8A8; height: 40px; border-radius: 30px; padding: 0 15px; margin-bottom: 25px; outline: none;">

                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" style="width: 100%; height: 60px; border: 1px solid #000000; border-radius: 50px; background-color: #000000; color: #FFF; cursor:pointer; outline:none;">{{ trans('frontend.reset.btn-redefinir') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection