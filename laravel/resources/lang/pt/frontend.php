<?php

return [

    'header' => [
        'idioma'            => 'IDIOMA',
        'ola'               => 'OLÁ',
        'pecas-favoritas'   => 'PEÇAS FAVORITAS',
    ],

    'modal' => [
        'login-titulo'       => 'OU FAÇA LOGIN ABAIXO',
        'senha'              => 'SENHA',
        'btn-prosseguir'     => 'PROSSEGUIR',
        'link-esqueci'       => 'esqueci minha senha',
        'criar-cadastro'     => 'SE VOCÊ AINDA NÃO FEZ SEU CADASTRO',
        'btn-criar-cadastro' => 'CRIAR MEU CADASTRO',
        'esq-senha-titulo'   => 'ESQUECI MINHA SENHA',
        'cadastro-titulo'    => 'PRIMEIRO CADASTRO',
        'repetir-senha'      => 'REPETIR SENHA',
        'nome'               => 'NOME',
        'telefone'           => 'TELEFONE',
        'pais'               => 'PAÍS',
        'redefinicao-senha'  => 'REDEFINIÇÃO DE SENHA',
    ],

    'home' => [
        'cronometro-faltam'        => 'Faltam',
        'cronometro-dias'          => 'dias',
        'cronometro-horas'         => 'horas e',
        'cronometro-minutos-aofim' => 'minutos para o encerramento do evento',
        'agenda-titulo'            => 'AGENDA DO EVENTO',
        'agenda-completa'          => 'AGENDA COMPLETA',
        'obtenha-ebook'            => 'OBTENHA O E-BOOK DA EDIÇÃO 2019',
        'assine-livro'             => 'ASSINE O LIVRO DO EVENTO 2020',
        'evento-encerrado'         => 'A edição 2020 do Brazil Jewelry Week já se encerrou. Esperamos por você em 2021!',
    ],

    'filtro' => [
        'expositores-titulo'       => 'ARTISTAS JOALHEIROS EXPOSITORES',
        'filtro-pais'              => 'país',
        'filtro-individuais'       => 'joalheiros individuais',
        'filtro-coletivos'         => 'coletivos | galerias | escolas',
    ],

    'expositor' => [
        'favoritar-joia'     => 'favoritar joia',
        'compartilhar-joia'  => 'compartilhar joia',
        'conversa-joalheiro' => 'iniciar conversa com este joalheiro',
        'conversa-coletivo'  => 'iniciar conversa com este contato',
        'instagram'          => 'conferir o instagram',
        'email'              => 'enviar e-mail',
        'website'            => 'visitar o website',
        'modal-compartilhar' => 'COMPARTILHAR',
    ],

    'agenda' => [
        'titulo'         => 'AGENDA COMPLETA DO EVENTO',
        'transmissao'    => 'Transmissão',
        'id-reuniao'     => 'ID da reunião',
        'senha'          => 'Senha de acesso',
        'ver-mais'       => 'VER MAIS',
        'horario-brasil' => 'horário do Brasil',
        'clique'         => 'clique',
    ],

    'favoritas' => [
        'titulo'             => 'LISTA DE PEÇAS FAVORITAS',
        'exluir'             => 'excluir dos favoritos',
        'compartilhar'       => 'compartilhar joia',
        'atualizar-cadastro' => 'MANTER MEU CADASTRO ATUALIZADO',
        'btn-atualizar'      => 'ATUALIZAR',
    ],

    'assine-livro' => [
        'titulo'         => 'ASSINE O LIVRO DO EVENTO',
        'deixe-mensagem' => 'DEIXE A SUA MENSAGEM',
        'frase-aviso'    => 'A assinatura será o nome e país que constam no seu cadastro conosco.',
        'btn-assinar'    => 'ASSINAR',
        'ver-mais'       => 'VER MAIS',
        'obrigado'       => 'OBRIGADO',
    ],

    'footer' => [
        'link-home'            => 'HOME ∙ EVENTO BRAZIL JEWELRY WEEK ∙',
        'link-agenda'          => 'AGENDA COMPLETA DO EVENTO ∙',
        'link-filtro'          => 'BUSCAR ARTISTAS JOALHEIROS EXPOSITORES ∙',
        'realizacao'           => 'REALIZAÇÃO',
        'patrocinadores'       => 'APOIADORES',
        'politica-privacidade' => 'POLÍTICA DE PRIVACIDADE',
        'direitos'             => 'TODOS OS DIREITOS RESERVADOS',
    ],

    'reset' => [
        'titulo'        => 'REDEFINIÇÃO DE SENHA',
        'btn-redefinir' => 'REDEFINIR SENHA',
        'recuperacao'   => 'Recuperação de senha',
        'ola'           => 'Olá',
        'frase'         => 'Você solicitou recuperação da sua senha em nosso sistema. As senhas são criptografadas e seguras, portanto você deve criar uma nova senha acessando o link',
        'clique-aqui'   => 'Clique aqui para redefinir sua senha.',
    ],

];
