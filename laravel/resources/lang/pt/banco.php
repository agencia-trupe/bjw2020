<?php

return [

    'texto_sobre'  => 'texto_sobre',
    'frase_sobre'  => 'frase_sobre',
    'evento_duracao' => 'evento_duracao',
    'frase_inicio' => 'frase_inicio',
    'frase_agenda' => 'frase_agenda',
    'frase_saudacao' => 'frase_saudacao',
    'evento_periodo' => 'evento_periodo',
    'dia_evento'   => 'dia_evento',
    'tipo'         => 'tipo',
    'nome'         => 'nome',
    'texto'        => 'texto',
    'titulo'       => 'titulo',
    'descricao'    => 'descricao',
    'nome_joia'    => 'nome_joia',
    'data'         => 'data'
    
];
