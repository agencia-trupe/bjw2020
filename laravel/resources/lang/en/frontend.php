<?php

return [

    'header' => [
        'idioma'            => 'LANGUAGE',
        'ola'               => 'HELLO',
        'pecas-favoritas'   => 'FAVORITE PIECES',
    ],

    'modal' => [
        'login-titulo'       => 'OR LOGIN BELOW',
        'senha'              => 'PASSWORD',
        'btn-prosseguir'     => 'PROCEED',
        'link-esqueci'       => 'forgot my password',
        'criar-cadastro'     => 'IF YOU HAVE NOT DONE YOUR REGISTRATION YET',
        'btn-criar-cadastro' => 'CREATE MY REGISTRATION',
        'esq-senha-titulo'   => 'FORGOT MY PASSWORD',
        'cadastro-titulo'    => 'FIRST REGISTRATION',
        'repetir-senha'      => 'REPEAT PASSWORD',
        'nome'               => 'NAME',
        'telefone'           => 'TELEPHONE',
        'pais'               => 'COUNTRY',
        'redefinicao-senha'  => 'PASSWORD RESET',
    ],

    'home' => [
        'cronometro-faltam'        => '',
        'cronometro-dias'          => 'days',
        'cronometro-horas'         => 'hours and',
        'cronometro-minutos-aofim' => 'minutes left before the event closes',
        'agenda-titulo'            => 'EVENT SCHEDULE',
        'agenda-completa'          => 'COMPLETE SCHEDULE',
        'obtenha-ebook'            => 'GET THE 2019 EDITION E-BOOK',
        'assine-livro'             => 'GET THE 2020 EVENT E-BOOK',
        'evento-encerrado'         => 'The 2020 edition of Brazil Jewelry Week is now over. We are waiting for you in 2021!',
    ],

    'filtro' => [
        'expositores-titulo'       => 'EXHIBITING JEWELRY ARTISTS',
        'filtro-pais'              => 'country',
        'filtro-individuais'       => 'individual jewelers',
        'filtro-coletivos'         => 'collective | galleries | schools',
    ],

    'expositor' => [
        'favoritar-joia'     => 'favorite jewel',
        'compartilhar-joia'  => 'share jewel',
        'conversa-joalheiro' => 'start a conversation with this jeweler',
        'conversa-coletivo'  => 'start conversation with this contact',
        'instagram'          => 'check instagram',
        'email'              => 'send e-mail',
        'website'            => 'visit website',
        'modal-compartilhar' => 'SHARE',
    ],

    'agenda' => [
        'titulo'         => 'COMPLETE EVENT SCHEDULE',
        'transmissao'    => 'Transmition',
        'id-reuniao'     => 'Reunion ID',
        'senha'          => 'Acess password',
        'ver-mais'       => 'SEE MORE',
        'horario-brasil' => 'Brazil time',
        'clique'         => 'click',
    ],

    'favoritas' => [
        'titulo'             => 'FAVORITE JEWELS LIST',
        'exluir'             => 'delete from favorites',
        'compartilhar'       => 'share jewel',
        'atualizar-cadastro' => 'KEEP MY REGISTRATION UPDATE',
        'btn-atualizar'      => 'UPDATE',
    ],

    'assine-livro' => [
        'titulo'         => 'SIGN THE EVENT BOOK',
        'deixe-mensagem' => 'LEAVE YOUR MESSAGE',
        'frase-aviso'    => 'The signature will be the name and country in your registration with us.',
        'btn-assinar'    => 'SIGN',
        'ver-mais'       => 'SEE MORE',
        'obrigado'       => 'THANKS',
    ],

    'footer' => [
        'link-home'            => 'HOME ∙ BRAZIL JEWELRY WEEK EVENT ∙',
        'link-agenda'          => 'COMPLETE EVENT SCHEDULE ∙',
        'link-filtro'          => 'SEARCH EXHIBITING JEWELRY ARTISTS ∙',
        'realizacao'           => 'REALIZATION',
        'patrocinadores'       => 'SPONSORS',
        'politica-privacidade' => 'PRIVACY POLICY',
        'direitos'             => 'ALL RIGHTS RESERVED',
    ],

    'reset' => [
        'titulo'        => 'PASSWORD RESET',
        'btn-redefinir' => 'REDEFINE PASSWORD',
        'recuperacao'   => 'Password recovery',
        'ola'           => 'Hello',
        'frase'         => 'You have requested recovery of your password on our system. Passwords are encrypted and secure, so you must create a new password by accessing the link',
        'clique-aqui'   => 'Click here to reset your password.',
    ],

];
