<?php

return [

    'texto_sobre'  => 'texto_sobre_en',
    'frase_sobre'  => 'frase_sobre_en',
    'evento_duracao' => 'evento_duracao_en',
    'frase_inicio' => 'frase_inicio_en',
    'frase_agenda' => 'frase_agenda_en',
    'frase_saudacao' => 'frase_saudacao_en',
    'evento_periodo' => 'evento_periodo_en',
    'dia_evento'   => 'dia_evento_en',
    'tipo'         => 'tipo_en',
    'nome'         => 'nome_en',
    'texto'        => 'texto_en',
    'titulo'       => 'titulo_en',
    'descricao'    => 'descricao_en',
    'nome_joia'    => 'nome_joia_en',
    'data'         => 'data_en'

];
