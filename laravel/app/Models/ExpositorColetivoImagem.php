<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ExpositorColetivoImagem extends Model
{
    protected $table = 'expositores_coletivos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeColetivo($query, $id)
    {
        return $query->where('coletivo_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1200,
            'height' => null,
            'path'   => 'assets/img/expositores-coletivos/imagens/'
        ]);
    }
}
