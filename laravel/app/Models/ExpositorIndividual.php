<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class ExpositorIndividual extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'expositores_individuais';

    protected $guarded = ['id'];

    public function joias()
    {
        return $this->hasMany('App\Models\ExpositorIndividualJoia', 'individual_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => 400,
            'path'   => 'assets/img/expositores/'
        ]);
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/expositores/fotos/'
        ]);
    }
}
