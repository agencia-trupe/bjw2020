<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = 'agenda';

    protected $guarded = ['id'];

    protected $dates = ['data'];

    public function palestras()
    {
        return $this->hasMany('App\Models\AgendaPalestra', 'agenda_id')->ordenados();
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }
}
