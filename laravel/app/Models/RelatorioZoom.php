<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatorioZoom extends Model
{
    protected $table = 'relatorio_zoom';

    protected $guarded = ['id'];

    public function cadastros()
    {
        return $this->hasMany('App\Models\Cadastro', 'cadastro_id')->ordenados();
    }

    public function palestras()
    {
        return $this->hasMany('App\Models\AgendaPalestra', 'palestra_id')->ordenados();
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }
}
