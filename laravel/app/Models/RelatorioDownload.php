<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatorioDownload extends Model
{
    protected $table = 'relatorio_download';

    protected $guarded = ['id'];

    public function cadastros()
    {
        return $this->hasMany('App\Models\Cadastro', 'cadastro_id')->ordenados();
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }
}
