<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ExpositorIndividualCampanha extends Model
{
    protected $table = 'expositores_individuais_campanha';

    protected $guarded = ['id'];

    public function scopeIndividual($query, $id)
    {
        return $query->where('individual_id', $id);
    }

    public static function uploadImagem1()
    {
        return CropImage::make('imagem1', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/expositores-individuais/campanha/'
        ]);
    }

    public static function uploadImagem2()
    {
        return CropImage::make('imagem2', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/expositores-individuais/campanha/'
        ]);
    }

    public static function uploadImagem3()
    {
        return CropImage::make('imagem3', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/expositores-individuais/campanha/'
        ]);
    }
}
