<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class ExpositorColetivo extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nome',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'expositores_coletivos';

    protected $guarded = ['id'];

    public function imagens()
    {
        return $this->hasMany('App\Models\ExpositorColetivoImagem', 'coletivo_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => 400,
            'path'   => 'assets/img/expositores/'
        ]);
    }
}
