<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastrosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'       => 'required',
            'telefone'   => 'required',
            'pais'       => 'required',
            'email'      => 'required|email',
            'senha'      => 'required|confirmed',
        ];

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email,' . auth('evento')->user()->id;
            $rules['senha'] = 'confirmed';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'nome.required'     => 'preencha seu nome',
            'email.required'    => 'insira um endereço de e-mail válido',
            'email.email'       => 'insira um endereço de e-mail válido',
            'telefone.required' => 'preencha seu telefone',
            'pais.required'     => 'preencha seu país',
            'senha.required'    => 'insira uma senha',
            'senha.confirmed'   => 'a confirmação de senha não confere',
            'senha.min'         => 'sua senha deve ter no mínimo 6 caracteres',
        ];
    }
}
