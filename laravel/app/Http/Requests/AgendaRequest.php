<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgendaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'dia_evento'    => 'required',
            'dia_evento_en' => 'required',
            'data'          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
