<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'        => 'required',
            'data_inicio' => 'required',
            'data_fim'    => 'required',
            'realizacao'  => 'required',
            'site'        => 'required',
            'telefone'    => 'required',
            'texto_sobre' => 'required',
            'texto_sobre_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
