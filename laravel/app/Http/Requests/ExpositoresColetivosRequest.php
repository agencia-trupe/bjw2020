<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExpositoresColetivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'      => 'required',
            'pais_id'   => 'required',
            'ano'       => 'required',
            'texto'     => 'required',
            // 'whatsapp'  => 'required',
            'instagram' => 'required',
            'email'     => 'required|email',
            'capa'      => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'nome.required' => 'Insira o nome do expositor',
            'pais_id.required' => 'Selecione um país',
            'required' => 'Preencha todos os campos corretamente',
            'email'    => 'Insira um e-mail válido',
            'image'    => 'Insira a imagem'
        ];
    }
}
