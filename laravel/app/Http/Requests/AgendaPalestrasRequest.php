<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgendaPalestrasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'horario'      => 'required',
            'tipo'         => 'required',
            'tipo_en'      => 'required',
            'titulo'       => 'required',
            'titulo_en'    => 'required',
            'nome'         => 'required',
            'descricao'    => 'required',
            'descricao_en' => 'required',
            'link'         => 'required',
            'id_reuniao'   => 'required',
            'senha_acesso' => 'required',
            'imagem'       => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.form-erro'),
        ];
    }
}
