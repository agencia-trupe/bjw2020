<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExpositoresIndividuaisJoiasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome_joia'      => 'required',
            'descricao' => 'required',
            'imagem1'   => 'image',
            'imagem2'   => 'image',
            'imagem3'   => 'image',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'image'    => 'Insira a imagem'
        ];
    }
}
