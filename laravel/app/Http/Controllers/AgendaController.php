<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use App\Models\AgendaPalestra;
use App\Models\Cadastro;
use App\Models\Pais;
use App\Models\RelatorioZoom;

class AgendaController extends Controller
{
    public function index()
    {
        $agendas = Agenda::orderBy('data', 'ASC')->get();

        $palestras = AgendaPalestra::ordenados()->get();

        $paises = Pais::all();

        return view('frontend.agenda', compact('agendas', 'palestras', 'paises'));
    }

    public function relatorio(Cadastro $user, AgendaPalestra $palestra)
    {
        RelatorioZoom::create([
            'cadastro_id' => $user->id,
            'palestra_id' => $palestra->id,
        ]);

        return redirect($palestra->link);
    }
}
