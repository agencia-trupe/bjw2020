<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ExpositorColetivo;
use App\Models\ExpositorIndividual;
use App\Models\Pais;

class FiltroController extends Controller
{
    public function index($pais)
    {
        $paises = Pais::all();

        if ($pais == 0) {
            $individuais = ExpositorIndividual::select('id', 'slug', 'nome', 'pais_id', 'capa')->orderBy('nome', 'ASC')->get();
            $coletivos = ExpositorColetivo::select('id', 'slug', 'nome', 'pais_id', 'capa')->orderBy('nome', 'ASC')->get();

            $todosExpositores = [$individuais, $coletivos];
            $expositores = [];

            foreach ($todosExpositores as $todos) {
                foreach ($todos as $exp) {
                    $expositores[] = $exp;
                }
            }
            return view('frontend.filtro', compact('paises', 'expositores', 'individuais', 'coletivos'));
            
        } else {
            $individuais = ExpositorIndividual::where('pais_id', $pais)->orderBy('nome', 'ASC')->get();
            $coletivos = ExpositorColetivo::where('pais_id', $pais)->orderBy('nome', 'ASC')->get();

            $todosExpositores = [$individuais, $coletivos];
            $expositores = [];

            foreach ($todosExpositores as $todos) {
                foreach ($todos as $exp) {
                    $expositores[] = $exp;
                }
            }
            return view('frontend.filtro', compact('paises', 'expositores', 'individuais', 'coletivos'));
        }
    }

    public function getExpositores($pais)
    {
        $paises = Pais::all();

        if ($pais == 0) {
            $individuais = ExpositorIndividual::select('id', 'slug', 'nome', 'pais_id', 'capa')->orderBy('nome', 'ASC')->get();
            $coletivos = ExpositorColetivo::select('id', 'slug', 'nome', 'pais_id', 'capa')->orderBy('nome', 'ASC')->get();

            $todosExpositores = [$individuais, $coletivos];
            $expositores = [];

            foreach ($todosExpositores as $todos) {
                foreach ($todos as $exp) {
                    $expositores[] = $exp;
                }
            }
            return response()->json(['expositores' => $expositores, 'individual' => $individuais, 'coletivo' => $coletivos, 'paises' => $paises]);
        } else {
            $expositoresInd = ExpositorIndividual::where('pais_id', $pais)->orderBy('nome', 'ASC')->get();
            $expositoresCol = ExpositorColetivo::where('pais_id', $pais)->orderBy('nome', 'ASC')->get();

            $todosExpositores = [$expositoresInd, $expositoresCol];
            $expositores = [];

            foreach ($todosExpositores as $todos) {
                foreach ($todos as $exp) {
                    $expositores[] = $exp;
                }
            }
            return response()->json(['expositores' => $expositores, 'individual' => $expositoresInd, 'coletivo' => $expositoresCol, 'paises' => $paises]);
        }
    }
}
