<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Assinatura;

class AssineLivroController extends Controller
{
    public function index()
    {
        $user = auth('evento')->user();

        $assinaturas = Assinatura::join('cadastros', 'cadastros.id', '=', 'assinaturas.cadastro_id')
            ->select('assinaturas.created_at as created_at', 'assinaturas.id as id', 'cadastros.nome as nome', 'cadastros.pais as pais', 'assinaturas.mensagem as mensagem')    
            ->orderBy('assinaturas.created_at', 'DESC')->get();

        return view('frontend.assine-livro', compact('user', 'assinaturas'));
    }

    public function post(Request $request, $user)
    {
        try {
            $input['mensagem'] = $request->mensagem;
            $input['cadastro_id'] = $request->user;

            Assinatura::create($input);

            $notification = array(
                'message' => 'Assinatura enviada com sucesso!',
                'alert-type' => 'success'
            );

            return redirect()->route('assine-livro', $user)->with($notification);
        } catch (\Exception $e) {

            $notification = array(
                'message' => 'Erro ao adicionar a assinatura.',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }
}
