<?php

namespace App\Http\Controllers;

use App\Models\ExpositorColetivo;
use App\Models\ExpositorColetivoImagem;
use App\Models\ExpositorIndividual;
use App\Models\ExpositorIndividualCampanha;
use App\Models\ExpositorIndividualJoia;
use App\Models\ExpositorIndividualProcesso;
use App\Models\Pais;

class ExpositoresController extends Controller
{
    public function index($slug)
    {
        $individual = ExpositorIndividual::where('slug', $slug)->first();

        $coletivo = ExpositorColetivo::where('slug', $slug)->first();

        $paises = Pais::all();

        if (!empty($individual)) {

            $joias = ExpositorIndividualJoia::where('individual_id', $individual->id)->ordenados()->get();
            $processo = ExpositorIndividualProcesso::where('individual_id', $individual->id)->first();
            $campanha = ExpositorIndividualCampanha::where('individual_id', $individual->id)->first();

            return view('frontend.expositor-individual', compact('individual', 'joias', 'processo', 'campanha', 'paises'));
        }
        if (!empty($coletivo)) {

            $imagens = ExpositorColetivoImagem::where('coletivo_id', $coletivo->id)->ordenados()->get();

            return view('frontend.expositor-coletivo', compact('coletivo', 'imagens', 'paises'));
        }
    }
}
