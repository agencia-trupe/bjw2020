<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpositoresIndividuaisJoiasRequest;
use App\Models\ExpositorIndividual;
use App\Models\ExpositorIndividualJoia;

class ExpositoresIndividuaisJoiasController extends Controller
{
    public function index(ExpositorIndividual $registro)
    {
        $joias = ExpositorIndividualJoia::individual($registro->id)->ordenados()->get();

        return view('painel.expositores-individuais.joias.index', compact('joias', 'registro'));
    }

    public function create(ExpositorIndividual $registro)
    {
        return view('painel.expositores-individuais.joias.create', compact('registro'));
    }

    public function store(ExpositoresIndividuaisJoiasRequest $request, ExpositorIndividual $registro)
    {
        try {
            $input = $request->all();
            $input['individual_id'] = $registro->id;

            if (isset($input['imagem1'])) $input['imagem1'] = ExpositorIndividualJoia::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = ExpositorIndividualJoia::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = ExpositorIndividualJoia::uploadImagem3();

            $count = count(ExpositorIndividualJoia::where('individual_id', $registro->id)->get());

            if ($count < 5) {
                ExpositorIndividualJoia::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique a quantidade de joias adicionadas (limite: 5 joias)']);
            }

            return redirect()->route('painel.expositores-individuais.joias.index', $registro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ExpositorIndividual $registro, $joia)
    {
        $joia = ExpositorIndividualJoia::find($joia);

        return view('painel.expositores-individuais.joias.edit', compact('registro', 'joia'));
    }

    public function update(ExpositoresIndividuaisJoiasRequest $request, ExpositorIndividual $registro, $joia)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem1'])) $input['imagem1'] = ExpositorIndividualJoia::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = ExpositorIndividualJoia::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = ExpositorIndividualJoia::uploadImagem3();

            $joia = ExpositorIndividualJoia::find($joia)->update($input);

            return redirect()->route('painel.expositores-individuais.joias.index', $registro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ExpositorIndividual $registro, $joia)
    {
        try {
            $joia = ExpositorIndividualJoia::find($joia);
            $joia->delete();

            return redirect()->route('painel.expositores-individuais.joias.index', $registro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
