<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Cadastro;

class CadastrosController extends Controller
{
    public function index()
    {
        $cadastros = Cadastro::orderBy('nome', 'ASC')->get();

        return view('painel.cadastros.index', compact('cadastros'));
    }
}
