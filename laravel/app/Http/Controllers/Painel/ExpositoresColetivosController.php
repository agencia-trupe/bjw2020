<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpositoresColetivosRequest;
use App\Models\ExpositorColetivo;
use App\Models\Pais;

class ExpositoresColetivosController extends Controller
{
    public function index()
    {
        $registros = ExpositorColetivo::orderBy('nome', 'ASC')->get();

        return view('painel.expositores-coletivos.index', compact('registros'));
    }

    public function create()
    {
        $paises = Pais::orderBy('nome', 'asc')->get();

        return view('painel.expositores-coletivos.create', compact('paises'));
    }

    public function store(ExpositoresColetivosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ExpositorColetivo::upload_capa();

            ExpositorColetivo::create($input);

            return redirect()->route('painel.expositores-coletivos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function show(ExpositorColetivo $registro)
    {
        $paises = Pais::orderBy('nome', 'asc')->get();
        // $registro = ExpositorColetivo::find($registro);

        return view('painel.expositores-coletivos.show', compact('registro', 'paises'));
    }

    public function edit(ExpositorColetivo $registro)
    {
        $paises = Pais::orderBy('nome', 'asc')->get();
        // $registro = ExpositorColetivo::find($registro);

        return view('painel.expositores-coletivos.edit', compact('registro', 'paises'));
    }

    public function update(ExpositoresColetivosRequest $request, ExpositorColetivo $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ExpositorColetivo::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.expositores-coletivos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ExpositorColetivo $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.expositores-coletivos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
