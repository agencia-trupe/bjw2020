<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpositoresIndividuaisRequest;
use App\Models\ExpositorIndividual;
use App\Models\Pais;

class ExpositoresIndividuaisController extends Controller
{
    public function index()
    {
        $registros = ExpositorIndividual::orderBy('nome', 'ASC')->get();

        return view('painel.expositores-individuais.index', compact('registros'));
    }

    public function create()
    {
        $paises = Pais::orderBy('nome', 'asc')->get();

        return view('painel.expositores-individuais.create', compact('paises'));
    }

    public function store(ExpositoresIndividuaisRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ExpositorIndividual::upload_capa();
            if (isset($input['foto'])) $input['foto'] = ExpositorIndividual::upload_foto();

            ExpositorIndividual::create($input);

            return redirect()->route('painel.expositores-individuais.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function show(ExpositorIndividual $registro)
    {
        $paises = Pais::orderBy('nome', 'asc')->get();
        // $registro = ExpositorIndividual::find($registro);

        return view('painel.expositores-individuais.show', compact('registro', 'paises'));
    }

    public function edit(ExpositorIndividual $registro)
    {
        $paises = Pais::orderBy('nome', 'asc')->get();
        // $registro = ExpositorIndividual::find($registro);

        return view('painel.expositores-individuais.edit', compact('registro', 'paises'));
    }

    public function update(ExpositoresIndividuaisRequest $request, ExpositorIndividual $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ExpositorIndividual::upload_capa();
            if (isset($input['foto'])) $input['foto'] = ExpositorIndividual::upload_foto();

            $registro->update($input);

            return redirect()->route('painel.expositores-individuais.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ExpositorIndividual $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.expositores-individuais.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
