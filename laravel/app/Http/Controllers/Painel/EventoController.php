<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventoRequest;
use App\Models\Evento;

class EventoController extends Controller
{
    public function index()
    {
        $registro = Evento::first();
        // dd($registro);

        return view('painel.evento.edit', compact('registro'));
    }

    public function update(EventoRequest $request, Evento $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.evento.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
