<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgendaPalestrasRequest;
use App\Models\Agenda;
use App\Models\AgendaPalestra;

class AgendaPalestrasController extends Controller
{
    public function index(Agenda $registro)
    {
        $palestras = AgendaPalestra::agenda($registro->id)->ordenados()->get();

        return view('painel.agenda.palestras.index', compact('palestras', 'registro'));
    }

    public function create(Agenda $registro)
    {
        return view('painel.agenda.palestras.create', compact('registro'));
    }

    public function store(AgendaPalestrasRequest $request, Agenda $registro)
    {
        try {
            $input = $request->all();
            $input['agenda_id'] = $registro->id;

            if (isset($input['imagem'])) $input['imagem'] = AgendaPalestra::uploadImagem();

            AgendaPalestra::create($input);

            return redirect()->route('painel.agenda.palestras.index', $registro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function show(Agenda $registro, $palestra)
    {
        $palestra = AgendaPalestra::find($palestra);

        return view('painel.agenda.palestras.show', compact('registro', 'palestra'));
    }

    public function edit(Agenda $registro, $palestra)
    {
        $palestra = AgendaPalestra::find($palestra);

        return view('painel.agenda.palestras.edit', compact('registro', 'palestra'));
    }

    public function update(AgendaPalestrasRequest $request, Agenda $registro, $palestra)
    {
        try {
            $input = $request->all();
            $input['agenda_id'] = $registro->id;

            if (isset($input['imagem'])) $input['imagem'] = AgendaPalestra::uploadImagem();

            $palestra = AgendaPalestra::find($palestra)->update($input);

            return redirect()->route('painel.agenda.palestras.index', $registro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Agenda $registro, $palestra)
    {
        try {
            $palestra = AgendaPalestra::find($palestra)->delete();

            return redirect()->route('painel.agenda.palestras.index', $registro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
