<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExpositoresIndividuaisCampanhaRequest;
use App\Models\ExpositorIndividual;
use App\Models\ExpositorIndividualCampanha;

class ExpositoresIndividuaisCampanhaController extends Controller
{
    public function index(ExpositorIndividual $registro)
    {
        $campanha = ExpositorIndividualCampanha::individual($registro->id)->first();

        return view('painel.expositores-individuais.campanha.index', compact('campanha', 'registro'));
    }

    public function create(ExpositorIndividual $registro)
    {
        return view('painel.expositores-individuais.campanha.create', compact('registro'));
    }

    public function store(ExpositoresIndividuaisCampanhaRequest $request, ExpositorIndividual $registro)
    {
        try {
            $input = $request->all();
            $input['individual_id'] = $registro->id;

            if (isset($input['imagem1'])) $input['imagem1'] = ExpositorIndividualCampanha::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = ExpositorIndividualCampanha::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = ExpositorIndividualCampanha::uploadImagem3();

            ExpositorIndividualCampanha::create($input);

            return redirect()->route('painel.expositores-individuais.campanha.index', $registro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ExpositorIndividual $registro, ExpositorIndividualCampanha $campanha)
    {
        return view('painel.expositores-individuais.campanha.edit', compact('registro', 'campanha'));
    }

    public function update(ExpositoresIndividuaisCampanhaRequest $request, ExpositorIndividual $registro, ExpositorIndividualCampanha $campanha)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem1'])) $input['imagem1'] = ExpositorIndividualCampanha::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = ExpositorIndividualCampanha::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = ExpositorIndividualCampanha::uploadImagem3();

            $campanha->update($input);

            return redirect()->route('painel.expositores-individuais.campanha.index', $registro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ExpositorIndividual $registro, ExpositorIndividualCampanha $campanha)
    {
        try {
            $campanha->delete();

            return redirect()->route('painel.expositores-individuais.campanha.index', $registro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
