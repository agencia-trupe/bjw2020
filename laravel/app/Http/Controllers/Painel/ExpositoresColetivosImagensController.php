<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpositoresColetivosImagensRequest;
use App\Models\ExpositorColetivo;
use App\Models\ExpositorColetivoImagem;

class ExpositoresColetivosImagensController extends Controller
{
    public function index(ExpositorColetivo $registro)
    {
        $imagens = ExpositorColetivoImagem::coletivo($registro->id)->ordenados()->get();

        return view('painel.expositores-coletivos.imagens.index', compact('imagens', 'registro'));
    }

    public function show(ExpositorColetivo $registro, ExpositorColetivoImagem $imagem)
    {
        return $imagem;
    }

    public function store(ExpositorColetivo $registro, ExpositoresColetivosImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ExpositorColetivoImagem::uploadImagem();
            $input['coletivo_id'] = $registro->id;

            $count = count(ExpositorColetivoImagem::where('coletivo_id', $registro->id)->get());

            if ($count < 5) {
                $imagem = ExpositorColetivoImagem::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique a quantidade de imagens adicionadas (limite: 5 joias)']);
            }

            $view = view('painel.expositores-coletivos.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(ExpositorColetivo $registro, $imagem)
    {
        try {
            $imagem = ExpositorColetivoImagem::find($imagem);
            $imagem->delete();

            return redirect()->route('painel.expositores-coletivos.imagens.index', $registro)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }
}
