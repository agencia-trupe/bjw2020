<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpositoresIndividuaisProcessoRequest;
use App\Models\ExpositorIndividual;
use App\Models\ExpositorIndividualProcesso;

class ExpositoresIndividuaisProcessoController extends Controller
{
    public function index(ExpositorIndividual $registro)
    {
        $processo = ExpositorIndividualProcesso::individual($registro->id)->first();

        return view('painel.expositores-individuais.processo.index', compact('processo', 'registro'));
    }

    public function create(ExpositorIndividual $registro)
    {
        return view('painel.expositores-individuais.processo.create', compact('registro'));
    }

    public function store(ExpositoresIndividuaisProcessoRequest $request, ExpositorIndividual $registro)
    {
        try {
            $input = $request->all();
            $input['individual_id'] = $registro->id;

            if (isset($input['imagem1'])) $input['imagem1'] = ExpositorIndividualProcesso::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = ExpositorIndividualProcesso::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = ExpositorIndividualProcesso::uploadImagem3();

            ExpositorIndividualProcesso::create($input);

            return redirect()->route('painel.expositores-individuais.processo.index', $registro->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ExpositorIndividual $registro, ExpositorIndividualProcesso $processo)
    {
        return view('painel.expositores-individuais.processo.edit', compact('registro', 'processo'));
    }

    public function update(ExpositoresIndividuaisProcessoRequest $request, ExpositorIndividual $registro, ExpositorIndividualProcesso $processo)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem1'])) $input['imagem1'] = ExpositorIndividualProcesso::uploadImagem1();
            if (isset($input['imagem2'])) $input['imagem2'] = ExpositorIndividualProcesso::uploadImagem2();
            if (isset($input['imagem3'])) $input['imagem3'] = ExpositorIndividualProcesso::uploadImagem3();

            $processo->update($input);

            return redirect()->route('painel.expositores-individuais.processo.index', $registro->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ExpositorIndividual $registro, ExpositorIndividualProcesso $processo)
    {
        try {
            $processo->delete();

            return redirect()->route('painel.expositores-individuais.processo.index', $registro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
