<?php

namespace App\Http\Controllers;

class NovaHomeController extends Controller
{
    public function index()
    {
        return view('frontend.nova-home');
    }
}
