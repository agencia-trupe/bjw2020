<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Agenda;
use App\Models\AgendaPalestra;
use App\Models\Cadastro;
use App\Models\Evento;
use App\Models\ExpositorColetivo;
use App\Models\ExpositorIndividual;
use App\Models\Pais;
use App\Models\RelatorioDownload;
use Illuminate\Http\Request as HttpRequest;

class HomeController extends Controller
{
    public function index()
    {
        $evento = Evento::first();
        $paises = Pais::all();

        $individuais = ExpositorIndividual::select('id', 'slug', 'nome', 'pais_id', 'capa')->get();
        $coletivos = ExpositorColetivo::select('id', 'slug', 'nome', 'pais_id', 'capa')->get();

        $todosExpositores = [$individuais, $coletivos];
        $expositores = [];

        foreach ($todosExpositores as $todos) {
            foreach ($todos as $exp) {
                $expositores[] = $exp;
            }
        }

        $agenda = Agenda::orderBy('data', 'ASC')->where('data', '>=', date('Y-m-d'))->first();
        
        if ($agenda != null) {
            $palestras = AgendaPalestra::where('agenda_id', $agenda->id)->get();
        }

        return view('frontend.home', compact('evento', 'paises', 'expositores', 'individuais', 'coletivos', 'agenda', 'palestras'));
    }

    public function download(Cadastro $user)
    {
        RelatorioDownload::create([
            'cadastro_id' => $user->id,
        ]);

        $ebookPath = public_path() . "/assets/ebook/" . "ebook-BJW2019.pdf";

        return response()->download($ebookPath);
    }
    
}
