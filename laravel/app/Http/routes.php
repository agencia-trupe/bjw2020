<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'NovaHomeController@index')->name('novahome');

    Route::group([
        'prefix' => '2020_acesso_BJW'
    ], function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('filtro/{pais}', 'FiltroController@index')->name('filtro');
        Route::get('expositores/{pais}', 'FiltroController@getExpositores')->name('getExpositores');
        Route::get('expositor/{slug}', 'ExpositoresController@index')->name('expositores');
        Route::get('agenda', 'AgendaController@index')->name('agenda');
        // USER EVENTO
        Route::get('assine-livro/{user}', 'AssineLivroController@index')->name('assine-livro');
        Route::post('assine-livro/{user}', 'AssineLivroController@post')->name('assine-livro.post');
        Route::get('favoritas/{user}', 'CadastrosFavoritasController@index')->name('favoritas');
        Route::post('favoritas/{user}/{joia}', 'CadastrosFavoritasController@favoritar')->name('favoritas.post');
        Route::delete('favoritas/{user}/{joia}', 'CadastrosFavoritasController@desfavoritar')->name('favoritas.delete');
        Route::get('ebook/download/{user}', 'HomeController@download')->name('ebook.download');
        Route::get('palestra/zoom/{user}/{palestra}', 'AgendaController@relatorio')->name('relatorio.zoom');
        Route::get('compartilhar/whatsapp/{user}/{joia}', 'CadastrosFavoritasController@relatorioWhatsapp')->name('relatorio.whatsapp.compartilhar');
        Route::get('compartilhar/facebook/{user}/{joia}', 'CadastrosFavoritasController@relatorioFacebook')->name('relatorio.facebook.compartilhar');
        Route::get('compartilhar/email/{user}/{joia}', 'CadastrosFavoritasController@relatorioEmail')->name('relatorio.email.compartilhar');
    });

    // Idiomas
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Login Evento
    Route::group([
        'prefix' => 'evento',
        'namespace' => 'Evento',
    ], function () {
        Route::post('login', 'Auth\AuthController@login')->name('evento.login.post');
        Route::post('cadastro', 'Auth\AuthController@create')->name('evento.register.post');
        Route::post('atualizar/{user}', 'Auth\AuthController@update')->name('evento.atualizar.user');
        Route::post('esqueci-minha-senha', 'Auth\PasswordController@sendResetLinkEmail')->name('evento.password-request.post');
        Route::get('redefinicao-de-senha/{token}', 'Auth\PasswordController@showResetForm')->name('evento.password-reset');
        Route::post('redefinicao-de-senha', 'Auth\PasswordController@reset')->name('evento.password-reset.post');
        Route::get('logout', 'Auth\AuthController@logout')->name('evento.logout');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('evento', 'EventoController', ['only' => ['index', 'update']]);
        Route::resource('agenda', 'AgendaController', ['except' => 'show']);
        Route::resource('agenda.palestras', 'AgendaPalestrasController');
        Route::resource('assinaturas-livro', 'AssinaturasLivroController', ['only' => ['index', 'destroy']]);
        Route::resource('expositores-coletivos', 'ExpositoresColetivosController');
        Route::resource('expositores-coletivos.imagens', 'ExpositoresColetivosImagensController');
        Route::resource('expositores-individuais', 'ExpositoresIndividuaisController');
        Route::resource('expositores-individuais.joias', 'ExpositoresIndividuaisJoiasController');
        Route::resource('expositores-individuais.processo', 'ExpositoresIndividuaisProcessoController');
        Route::resource('expositores-individuais.campanha', 'ExpositoresIndividuaisCampanhaController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');
        Route::get('cadastros', 'CadastrosController@index')->name('painel.cadastros.index');
        Route::get('relatorios/download-ebook', 'RelatoriosController@downloadEbook')->name('painel.relatorios.download-ebook');
        Route::get('relatorios/palestra-zoom', 'RelatoriosController@palestraZoom')->name('painel.relatorios.palestra-zoom');
        Route::get('relatorios/compartilhar-joia', 'RelatoriosController@compartilharJoia')->name('painel.relatorios.compartilhar-joia');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
