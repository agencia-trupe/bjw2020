<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracao::first());
        });

        view()->composer('frontend.common.*', function ($view) {
            $view->with('evento', \App\Models\Evento::first());
        });

        view()->composer('frontend.common.*', function ($view) {
            $view->with('cadastros', \App\Models\Cadastro::all());
        });

        view()->composer('frontend.*', function () {
            
            if (session()->get('locale') == "pt" || session()->get('locale') == null) {
                setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            } else {
                setlocale(LC_TIME, 'en', 'en-US');
            }
        });
    }

    public function register()
    {
        // 
    }
}
