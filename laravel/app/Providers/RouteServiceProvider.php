<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('agenda', 'App\Models\Agenda');
        $router->model('agenda_palestras', 'App\Models\AgendaPalestra');
        $router->model('expositores_coletivos', 'App\Models\ExpositorColetivo');
        $router->model('expositores_coletivos_imagens', 'App\Models\ExpositorColetivoImagem');
        $router->model('expositores_individuais', 'App\Models\ExpositorIndividual');
        $router->model('expositores_individuais_joias', 'App\Models\ExpositorIndividualJoia');
        $router->model('expositores_individuais_processo', 'App\Models\ExpositorIndividualProcesso');
        $router->model('expositores_individuais_campanha', 'App\Models\ExpositorIndividualCampanha');
        $router->model('evento', 'App\Models\Evento');
        $router->model('paises', 'App\Models\Pais');
        $router->model('cadastros', 'App\Models\Cadastro');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('assinaturas', 'App\Models\Assinatura');
        $router->model('cadastros_favoritas', 'App\Models\CadastroFavorita');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('coletivo_slug', function ($slug) {
            return \App\Models\ExpositorColetivo::whereSlug($slug)->firstOrFail();
        });

        $router->bind('individual_slug', function ($slug) {
            return \App\Models\ExpositorIndividual::whereSlug($slug)->firstOrFail();
        });

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
